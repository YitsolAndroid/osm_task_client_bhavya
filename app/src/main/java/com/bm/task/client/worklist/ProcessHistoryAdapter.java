/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import java.text.ParseException;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bm.task.client.utils.FontType;
import com.bm.task.client.xml.FormatDateTime;
import com.example.osm_xmlapi.R;

@SuppressLint("SimpleDateFormat")
public class ProcessHistoryAdapter extends BaseAdapter {

	private Context con;
	private List<ProcessHistoryDomain> lst;
	private FormatDateTime fDateTime;

	public ProcessHistoryAdapter(Context con, List<ProcessHistoryDomain> lst) {
		this.con = con;
		this.lst = lst;
	}

	@Override
	public int getCount() {
		return lst.size();

	}

	@Override
	public Object getItem(int position) {
		return lst.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		if (v == null) {
			LayoutInflater in = (LayoutInflater) con
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = in.inflate(R.layout.txt_process_history1, null);

		}
		
		ViewGroup root = (ViewGroup) v.findViewById(R.id.ll_txt_processHistory);
		new FontType(con, root);
		
		TextView txtActulDuration = (TextView) v
				.findViewById(R.id.txt_ActualDuration);
		TextView txtStartDate = (TextView) v.findViewById(R.id.txt_started);
		TextView txtCompleted = (TextView) v.findViewById(R.id.txt_complted);
		TextView txtTask = (TextView) v.findViewById(R.id.txt_Task);

		fDateTime=new FormatDateTime();

		String startDate = "--";
		String completed = "--";
		int actualDuration=0;
		
		try {
			if (lst.get(position).getCompleted() != "")
				completed = fDateTime.getFormattedDateTime(lst.get(position).getCompleted());
			
			if (lst.get(position).getStarted() != "")
				System.out.println("coming"+lst.size());
				startDate = fDateTime.getFormattedDateTime(lst.get(position).getStarted());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String duration;
		actualDuration = Integer.parseInt(lst.get(position).getActualDuration());
		duration=fDateTime.getSecondConverter(actualDuration);
		
				

		txtTask.setText("" + lst.get(position).getTask());
		txtStartDate.setText("Started : " + startDate);
		txtCompleted.setText("Completed : " + completed);
		txtActulDuration.setText(duration);
		
		if (position % 2 == 1) {

			v.setBackgroundColor(Color.parseColor("#e2e2e2"));
		} else {
			v.setBackgroundColor(Color.parseColor("#ffffff"));
		}

		return v;

	}
}
