package com.bm.task.client.utils;


import src.main.java.com.bm.task.client.connection.CheckConnectivity;

public class ServerUrl {
	
	public static String serverUrl = "http://"+ CheckConnectivity.server+":"+CheckConnectivity.port+"/OSMTaskClientRestWS/orderServices/";
	public static String serverUrlXml = "http://"+CheckConnectivity.server+":"+CheckConnectivity.port+"/OSMTaskClientRestWS/xml_api/";
	

}
