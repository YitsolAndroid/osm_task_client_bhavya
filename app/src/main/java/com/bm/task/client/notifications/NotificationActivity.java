/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.notifications;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Spinner;

import com.bm.task.client.utils.FontType;
import com.bm.task.client.worklist.SpnAdapter;
import com.example.osm_xmlapi.R;

public class NotificationActivity extends Fragment{
	
	public static String selected;

	private int lastExpandedPosition = -1;


	private Spinner spnSelect;
	
	private String[] spinnerArray={"Editor","Process History","Add Remark","Notification History","Acknowledgement"};

	private Context context;

	private Button btnRefresh;

	private ExpandableListView expndListNotification;
	
	public NotificationActivity(Context context) {
		this.context = context;
	}



	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.notifications, container, false);
	         
	    

		expndListNotification = (ExpandableListView) rootView.findViewById(R.id.lst_notification);


		btnRefresh = (Button)  rootView.findViewById(R.id.btn_refresh);
		
		
		spnSelect = (Spinner) rootView.findViewById(R.id.spn_select);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		ViewGroup root = (ViewGroup)  rootView.findViewById(R.id.ll_worklist);
		new FontType(context, root);
		
		spnSelect.setAdapter(new SpnAdapter(context, spinnerArray));
		
		spnSelect.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				
				selected=SpnAdapter.lst[pos];
				System.out.println(selected);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
	/*	Encryption encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(NextActivity.userName+":"+NextActivity.password);
			new GetProcessServer(context,expndListNotification,encoded).execute(ServerUrl.serverUrl +"getProcessofOrders");
		}catch(Exception e){
			
		}*/
		
		btnRefresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				/*Intent intent = new Intent(context,
						NotificationActivity.class);
				startActivity(intent);*/

			}
		});
		
		expndListNotification.setOnGroupExpandListener(new OnGroupExpandListener() {

		    public void onGroupExpand(int groupPosition) {
		    
		            if (lastExpandedPosition != -1
		                    && groupPosition != lastExpandedPosition) {
		                expndListNotification.collapseGroup(lastExpandedPosition);
		            }
		            lastExpandedPosition = groupPosition;
		    }
		});
		


		
	    return rootView;
    

	}

	public void onBackPressed() {

		

	}


}
