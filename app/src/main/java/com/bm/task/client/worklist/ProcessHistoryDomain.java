/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

public class ProcessHistoryDomain {
	
	private String task;
	private String expectedCompletionDate;
	private String actualDuration;
	private String exception;
	private String state;
	private String started;
	private String completed;
	private String status;
	private String user;
	private String taskID;
	private String taskType;
	private String parentTaskOrderHistID;
	private String dataNodeIndex;
	private String dataNodeMnemonic;
	private String dataNodeValue;
	private String orderHistID;
	private String fromOrderHistID;
	private String executionMode;
	private String transitionType;
	
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public String getExpectedCompletionDate() {
		return expectedCompletionDate;
	}
	public void setExpectedCompletionDate(String expectedCompletionDate) {
		this.expectedCompletionDate = expectedCompletionDate;
	}
	public String getActualDuration() {
		return actualDuration;
	}
	public void setActualDuration(String actualDuration) {
		this.actualDuration = actualDuration;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStarted() {
		return started;
	}
	public void setStarted(String started) {
		this.started = started;
	}
	public String getCompleted() {
		return completed;
	}
	public void setCompleted(String completed) {
		this.completed = completed;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getTaskID() {
		return taskID;
	}
	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public String getParentTaskOrderHistID() {
		return parentTaskOrderHistID;
	}
	public void setParentTaskOrderHistID(String parentTaskOrderHistID) {
		this.parentTaskOrderHistID = parentTaskOrderHistID;
	}
	public String getDataNodeIndex() {
		return dataNodeIndex;
	}
	public void setDataNodeIndex(String dataNodeIndex) {
		this.dataNodeIndex = dataNodeIndex;
	}
	public String getDataNodeMnemonic() {
		return dataNodeMnemonic;
	}
	public void setDataNodeMnemonic(String dataNodeMnemonic) {
		this.dataNodeMnemonic = dataNodeMnemonic;
	}
	public String getDataNodeValue() {
		return dataNodeValue;
	}
	public void setDataNodeValue(String dataNodeValue) {
		this.dataNodeValue = dataNodeValue;
	}
	public String getOrderHistID() {
		return orderHistID;
	}
	public void setOrderHistID(String orderHistID) {
		this.orderHistID = orderHistID;
	}
	public String getFromOrderHistID() {
		return fromOrderHistID;
	}
	public void setFromOrderHistID(String fromOrderHistID) {
		this.fromOrderHistID = fromOrderHistID;
	}
	public String getExecutionMode() {
		return executionMode;
	}
	public void setExecutionMode(String executionMode) {
		this.executionMode = executionMode;
	}
	
	public String getTransitionType() {
		return transitionType;
	}
	public void setTransitionType(String transitionType) {
		this.transitionType = transitionType;
	}
}
