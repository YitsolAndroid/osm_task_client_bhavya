package com.bm.task.client.restWS.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.ServerUrl;

import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;
import src.main.java.com.bm.task.client.xml.XMLParser;

@SuppressLint("ClickableViewAccessibility")
public class UpdateOrderServer extends AsyncTask<String, Void, String> {

	ProgressDialog progressDialog;
	String result;
	Context context;
	JSONObject json;
	private String userName;
	private String password;
	private String id;
	private String updateXml;
	private String status;
	private String encoded;
	private String task;
	private String xml;
	private String tabSelected;
	private static final String KEY_ITEM_Error = "error";
	private static final String KEY_ITEM_Error1 = "UpdateOrder.Error";
	private static final String Error = "Error";

	public UpdateOrderServer(Context context, String id, String updateXml,
			String userName, String password, String status, String task,
			String encoded,String tabSelected) {
		this.userName = userName;
		this.password = password;
		this.id = id;
		this.updateXml = updateXml;
		this.context = context;
		this.status = status;
		this.encoded = encoded;
		this.task = task;
		this.tabSelected = tabSelected;
	}

	protected void onPreExecute() {

		progressDialog = new ProgressDialog(context, R.style.MyTheme);
		progressDialog.setCancelable(false);
		progressDialog
				.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		progressDialog.show();

	}

	@Override
	protected String doInBackground(String... urls) {
		try {

			System.out.println(":::::::::::Coming Here :::::::::::::"
					+ updateXml + id + encoded + CheckConnectivity.server +CheckConnectivity.port
					);
			// make web service connection
			CheckConnectivity checkConnectivity = new CheckConnectivity();
			if(checkConnectivity.Connectivity() == true){
			HttpPost request = new HttpPost(urls[0]);
			request.setHeader("Auth", encoded);
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");
			request.setHeader("Accept-Encoding", "gzip");
			// Build JSON string
			JSONStringer TestApp = new JSONStringer().object()
					.key("publicIP").value(CheckConnectivity.server)
					.key("port").value(CheckConnectivity.port)
					.key("orderId").value(id)
					.key("root").value(updateXml)
					.endObject();
			StringEntity entity = new StringEntity(TestApp.toString());

			request.setEntity(entity);

			// Log.d("****Parameter Input****", "Testing:" + TestApp);
			StringBuilder sb = new StringBuilder();
			// Send request to WCF service
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);

			if(response.getStatusLine().toString().equalsIgnoreCase("HTTP/1.1 200 OK"))
			{
				// Get the status of web service
				BufferedReader br = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				// print status in log
				String line = br.readLine();
				sb.append(line);
				result = sb.toString();
			}
			else
			{
				
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", response.getStatusLine().toString());
				result = jsonObject.toString();
			}
			}else
			{
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", "OSM is not connected");
				result = jsonObject.toString();
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public void onPostExecute(String result) {

		progressDialog.dismiss();
		System.out.println("Updated ORDER =      ===" + result);
		try {
			JSONObject json = new JSONObject(result);
			xml = XML.toString(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(xml.startsWith("<status>")){
			xml = "<error>"+xml+"</error>";
		}
		System.out.println("My XML is ::: ::: "+xml);  
		XMLParser xmlParser = new XMLParser();
		Document doc = xmlParser.getDomElement(xml);
       

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		NodeList error1 = doc.getElementsByTagName(KEY_ITEM_Error1);
		if (error.getLength() >= 1 || error1.getLength() >= 1) {
			if(error.getLength()>=1){
					String desc,code;
					Element e = null;
					
					for (int position = 0; position < error.getLength(); position++) {
						e = (Element) error.item(position);
					}
					
					code = xmlParser.getValue(e, "status");
					desc = xmlParser.getValue(e, "errorMsg");
					
					if(desc.equalsIgnoreCase("")){
				
					CommonPopUp commonPopUp = new CommonPopUp(
							context,"\nOrderID : "+id+"\nError Description : "+code+"\n");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
					}else{
						CommonPopUp commonPopUp = new CommonPopUp(
								context,"\nOrderID : "+id+"\nError Code : "+code +"\nDescription : "+desc	+"\n");
						commonPopUp.setCanceledOnTouchOutside(true);
						commonPopUp.show();
					
					}
				
			}
			if(error1.getLength()>=1){
			String desc,code;
			Element e = null;
			NodeList nl = doc.getElementsByTagName(Error);
			
			for (int position = 0; position < nl.getLength(); position++) {
				e = (Element) nl.item(position);
			}
			
			desc = xmlParser.getValue(e, "desc");
			code = xmlParser.getValue(e, "code");
		
			CommonPopUp commonPopUp = new CommonPopUp(
					context,"\nid : "+id+"\nError Code : "+code +"\nDescription : "+desc	+"\n");
			commonPopUp.setCanceledOnTouchOutside(true);
			commonPopUp.show();
			}
			
		}

		else{
	

			Encryption encryption = new Encryption();
			try {
				String encoded = encryption
						.encrypt(FragmentDisplayActivity.userName + ":"
								+ FragmentDisplayActivity.password);

				new GetCompleteOrderServer(context,
						FragmentDisplayActivity.userName,
						FragmentDisplayActivity.password, id, CheckConnectivity.server,
						CheckConnectivity.port, status, task, encoded,tabSelected)
						.execute(ServerUrl.serverUrlXml + "getCompleteOrder");

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		
	}
}
