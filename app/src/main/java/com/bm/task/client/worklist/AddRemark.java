package com.bm.task.client.worklist;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.fragment.FragmentForWorkList;
import com.bm.task.client.query.QueryExpndListAdaptr;
import com.bm.task.client.restWS.server.AddRemarkServer;
import com.bm.task.client.restWS.server.GetOrderServerXML;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.utils.ServerUrl;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

@SuppressLint("ValidFragment")
public class AddRemark extends Fragment implements OnClickListener {

	private Button btnSave;
	private Button btnAttachment;
	private TextView txtOrderId;
	private TextView txtRef;
	private EditText edtRemark;
	private String attachmentFile;
	private Context context;
	private Intent in;
	private Button btnEditOrder;
	private ViewGroup root;
	private String menu;
	private ConnectionDetector connectionDetector;
	 private static final int PICK_FROM_GALLERY = 101;
	 
	 public AddRemark(Context context,String menu){
		 this.context = context;
		 this.menu = menu;
	 }

	@Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.add_remark, container, false);
	        
	        
	    root = (ViewGroup)rootView.findViewById(R.id.ll_addRemark);
	    new FontType(context, root);
		

		btnAttachment = (Button) rootView.findViewById(R.id.btn_Attachment);
		btnSave = (Button) rootView.findViewById(R.id.btn_Save);
		btnEditOrder = (Button) rootView.findViewById(R.id.btn_EditOrder);
		txtOrderId = (TextView) rootView.findViewById(R.id.txt_Remark_orderId);
		txtRef = (TextView) rootView.findViewById(R.id.txt_remark_Ref);
		edtRemark = (EditText) rootView.findViewById(R.id.edt_Remark);
		
		if(menu.equalsIgnoreCase("worklist")){

		txtOrderId.setText("Order ID : " + WorkExpndListAdaptr.id);
		txtRef.setText("Ref #. : " + WorkExpndListAdaptr.ref);
		}
		
		if(menu.equalsIgnoreCase("query")){
		txtOrderId.setText("Order ID : " + QueryExpndListAdaptr.id);
		txtRef.setText("Ref #. : " + QueryExpndListAdaptr.ref);
		}

		btnAttachment.setOnClickListener(this);
		btnSave.setOnClickListener(this);
		btnEditOrder.setOnClickListener(this);
		
		return rootView;

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btn_Attachment:
			openAttachment();
			break;

		case R.id.btn_Save:
			connectionDetector = new ConnectionDetector(context);
			if (!connectionDetector.isConnectingToInternet()) {
				CommonPopUp commonPopUp = new CommonPopUp(
						context, "Please activate internet...");
				commonPopUp.setCanceledOnTouchOutside(true);
				commonPopUp.show();
			}else{
			addRemark();
			}
			

			break;
		case R.id.btn_EditOrder:
			connectionDetector = new ConnectionDetector(context);
			if (!connectionDetector.isConnectingToInternet()) {
				CommonPopUp commonPopUp = new CommonPopUp(
						context, "Please activate internet...");
				commonPopUp.setCanceledOnTouchOutside(true);
				commonPopUp.show();
			}else{
			Encryption encryption = new Encryption();
			try {
				String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
			
			new GetOrderServerXML(context, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
					WorkExpndListAdaptr.id, CheckConnectivity.server,CheckConnectivity.port,"true",WorkExpndListAdaptr.task,encoded,WorkExpndListAdaptr.user).
					execute(ServerUrl.serverUrlXml + "getOrder");
			
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			in = new Intent(context,
					FragmentForWorkList.class);
			in.putExtra("selected", "editor");
			in.putExtra("flag", 1);
			context.startActivity(in);
			}
			break;

		
		}

	}

	private void openAttachment() {
		Intent intent = new Intent();

		intent.setType("image/*");

		intent.setAction(Intent.ACTION_GET_CONTENT);

		intent.putExtra("return-data", true);

		startActivityForResult(

		Intent.createChooser(intent, "Complete action using"),

		PICK_FROM_GALLERY);

	}

	private void addRemark() {
		String remark = edtRemark.getText().toString();
		
		
		
		
		try {
			Encryption encryption = new Encryption();
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
		
			
			System.out.println("The encode String is : "+encoded);
		new AddRemarkServer(context,CheckConnectivity.server,CheckConnectivity.port,WorkExpndListAdaptr.id,
				remark,attachmentFile,encoded)
		.execute(ServerUrl.serverUrlXml + "addRemark");
		
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		

		
	}
	
public void onBackPressed() {
		
		Intent intent = new Intent(context,
				FragmentDisplayActivity.class);
		intent.putExtra("selected", "worklist");
		intent.putExtra("flag", 1);
		startActivity(intent);

		

	}


  

}
