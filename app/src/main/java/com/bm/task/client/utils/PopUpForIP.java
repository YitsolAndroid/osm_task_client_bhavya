/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.osm_xmlapi.R;

public class PopUpForIP extends Activity implements android.view.View.OnClickListener{
	private TextView tv_Msg;
	private Button btnSettings;
	private String msg="Please Enter Proper IP"+"\n"+"To Enter Proper IP"+"\n"+"Please Click On SETTINGS";
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.common_popup_ip);

		
		tv_Msg = (TextView) findViewById(R.id.tv_Msg);
		tv_Msg.setText(""+msg);
		
		btnSettings = (Button) findViewById(R.id.button_settings);
		btnSettings.setText(""+"Settings");
		
		btnSettings.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.button_settings:
			
			Intent intent = new Intent(PopUpForIP.this,
					ChangeIP.class);
			startActivity(intent);
			
			break;

		default:
			break;
		}
		
	}
}
