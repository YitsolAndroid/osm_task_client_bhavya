/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.query.QueryExpndListAdaptr;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.xml.FormatDateTime;
import com.bm.task.client.xml.WorkListDomain;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.xml.XMLParser;

@SuppressLint({"SimpleDateFormat", "ValidFragment"})
public class ProcessHistoryActivity extends Fragment {

	private XMLParser xmlParser;
	private Document doc;

	private TextView txtPhOrderId;
	private TextView txtPhRefNo;
	private TextView txtPhExpected;
	private TextView txtPhActualDuration;
	private TextView txtPhStarted;

	private WorkListDomain workListDomain;
	private ProcessHistoryDomain processHistoryDomain;
	public static ArrayList<WorkListDomain> lst = new ArrayList<WorkListDomain>();
	public static ArrayList<WorkListDomain> lstSummary = new ArrayList<WorkListDomain>();
	public static ArrayList<ProcessHistoryDomain> finalList = new ArrayList<ProcessHistoryDomain>();
	public static ArrayList<ProcessHistoryDomain> tempList = new ArrayList<ProcessHistoryDomain>();
	public static ArrayList<String> taskList = new ArrayList<String>();
	public static ArrayList<String> startedList = new ArrayList<String>();

	private int actualDuration;
	private String taskName;
	private String started;
	private String completed;

	static final String KEY_ITEM_SUMMARY = "Summary";
	static final String KEY_ITEM_TRANSITION = "Transition";

	private ListView lstProcessHistory;
	private String xml;
	private Context context;
	
	public ProcessHistoryActivity(Context context,String xml)
	{
		this.context=context;
		this.xml=xml;
	}

	@Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.process_history, container, false);
		
		
		ViewGroup root = (ViewGroup) rootView.findViewById(R.id.ll_processHistory);
		new FontType(context, root);

		lstProcessHistory = (ListView)  rootView.findViewById(R.id.lst_ProcessHistory);

		txtPhActualDuration = (TextView)  rootView.findViewById(R.id.txt_ph_Actualduration);
		txtPhOrderId = (TextView)  rootView.findViewById(R.id.txt_ph_OrderId);
		txtPhRefNo = (TextView)  rootView.findViewById(R.id.txt_ph_Ref);
		txtPhExpected = (TextView)  rootView.findViewById(R.id.txt_ph_Excepted);
		txtPhStarted = (TextView)  rootView.findViewById(R.id.txt_ph_started);

	
		
	
			txtPhOrderId.setText("OrderID : " + WorkExpndListAdaptr.id);
			txtPhRefNo.setText("Ref #. : " + WorkExpndListAdaptr.ref);
	

		lst.clear();
		finalList.clear();
		lstSummary.clear();
		tempList.clear();
		taskList.clear();
		
		xmlParser = new XMLParser();
		doc = xmlParser.getDomElement(xml);
		NodeList nlSummary = doc.getElementsByTagName(KEY_ITEM_SUMMARY);
		Element eleSummary = (Element) nlSummary.item(0);
		NodeList nlChild = eleSummary.getChildNodes();
		for (int j = 0; j < nlChild.getLength(); j++) {
			System.out.println("coming here");
			String nodeName = nlChild.item(j).getNodeName();
			Element eleChild = (Element) nlChild.item(j);
			String nodeValue = xmlParser.getElementValue(eleChild);
			workListDomain = new WorkListDomain();
			workListDomain.setNodeValue(nodeValue);
			workListDomain.setNodeName(nodeName);
			lstSummary.add(workListDomain);

			if (nodeName.equalsIgnoreCase("ExpectedCompletionDate")) {

				try {
					FormatDateTime fDateTime=new FormatDateTime();
					String Expected = fDateTime.getFormattedDateTime(nodeValue);
					txtPhExpected.setText("Expected Completion Date : "
							+ Expected);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			if (nodeName.equalsIgnoreCase("StartDate")) {
				try {
					FormatDateTime fDateTime=new FormatDateTime();
					String started = fDateTime.getFormattedDateTime(nodeValue);
					txtPhStarted.setText("Start Date : " + started);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			if (nodeName.equalsIgnoreCase("ActualDuration")) {
				FormatDateTime fDateTime=new FormatDateTime();
				String actualDuration=fDateTime.getSecondConverter(Integer.parseInt(nodeValue));
				txtPhActualDuration.setText(actualDuration);

			}
		}

		NodeList nlTransition = doc.getElementsByTagName(KEY_ITEM_TRANSITION);
		processHistory(nlTransition, lst);

		lstProcessHistory.setAdapter(new ProcessHistoryAdapter(
				context, finalList));
		
		
		/**
		 * minimizing Duplicate items
		 */
		Set<String> checkDuplicates = new HashSet<String>();
		for (int i = 0; i < finalList.size(); i++) {
			String items = finalList.get(i).getTask();
			if (!checkDuplicates.add(items)) {
				// retain the item from set interface
				System.out.println("Duplicate in that list " + items);
			} else {
				taskList.add(items);
			}
		}

		for (int i = 0; i < taskList.size(); i++) {
			actualDuration = 0;
			startedList.clear();
			for (int j = 0; j < finalList.size(); j++) {
				
				System.out.println("my value   :  " +finalList.get(j)
				.getStarted());
				if (taskList.get(i)
						.equalsIgnoreCase(finalList.get(j).getTask())) {
					taskName = finalList.get(j).getTask();
					completed = finalList.get(j).getCompleted();
					started = finalList.get(j).getStarted();
					startedList.add(started);
					actualDuration = actualDuration
							+ Integer.parseInt(finalList.get(j)
									.getActualDuration());

				}

			}
			processHistoryDomain = new ProcessHistoryDomain();
			processHistoryDomain.setActualDuration(String
					.valueOf(actualDuration));
			processHistoryDomain.setCompleted(completed);
			processHistoryDomain.setStarted(startedList.get(0));
			processHistoryDomain.setTask(taskName);
			tempList.add(processHistoryDomain);
			System.out.println("actual : " + actualDuration);
		}

		lstProcessHistory.setAdapter(new ProcessHistoryAdapter(
				context, tempList));

		lstProcessHistory.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				String task = tempList.get(position).getTask();
				Intent in = new Intent(context,
						HistoryDescription.class);
				in.putExtra("task", task);
				startActivity(in);

			}
		});

		
		return rootView;

	}

	/**
	 * Getting all values in Transition Node in XMLAPI
	 * @param nl
	 * @param lst
	 */
	private void processHistory(NodeList nl, ArrayList<WorkListDomain> lst) {
		lst.clear();
		for (int i = 0; i < nl.getLength(); i++) {
			lst.clear();
			Element eleTransition = (Element) nl.item(i);
			NodeList nlChild = eleTransition.getChildNodes();

			for (int j = 0; j < nlChild.getLength(); j++) {
				String nodeName = nlChild.item(j).getNodeName();
				Element eleChild = (Element) nlChild.item(j);
				String nodeValue = xmlParser.getElementValue(eleChild);
				workListDomain = new WorkListDomain();
				workListDomain.setNodeValue(nodeValue);
				workListDomain.setNodeName(nodeName);
				lst.add(workListDomain);

				if (j == nlChild.getLength() - 1) {

					processHistoryDomain = new ProcessHistoryDomain();
					for(int l=0;l<=17;l++){
						System.out.println("startdate :::::::"+lst.get(l).getNodeName()+"\n");
					}
					
					processHistoryDomain.setUser(lst.get(0).getNodeValue());
					processHistoryDomain.setTask(lst.get(1).getNodeValue());
					processHistoryDomain.setDataNodeIndex(lst.get(2)
							.getNodeValue());
					processHistoryDomain.setOrderHistID(lst.get(3)
							.getNodeValue());
					processHistoryDomain.setDataNodeValue(lst.get(4)
							.getNodeValue());
					processHistoryDomain.setTaskID(lst.get(5).getNodeValue());
					processHistoryDomain.setActualDuration(lst.get(6)
							.getNodeValue());
					processHistoryDomain.setTransitionType(lst.get(7)
							.getNodeValue());
					processHistoryDomain.setDataNodeMnemonic(lst.get(8)
							.getNodeValue());
					processHistoryDomain.setExpectedCompletionDate(lst.get(9)
							.getNodeValue());
					processHistoryDomain.setExecutionMode(lst.get(10)
							.getNodeValue());
					processHistoryDomain.setStatus(lst.get(11).getNodeValue());
					processHistoryDomain.setStarted(lst.get(12).getNodeValue());
					processHistoryDomain.setState(lst.get(13).getNodeValue());
					processHistoryDomain.setFromOrderHistID(lst.get(14)
							.getNodeValue());
					processHistoryDomain.setParentTaskOrderHistID(lst.get(15)
							.getNodeValue());
					processHistoryDomain.setTaskType(lst.get(16).getNodeValue());
					processHistoryDomain
							.setCompleted(lst.get(17).getNodeValue());
					finalList.add(processHistoryDomain);
				}
			}

		}

	}
	
public void onBackPressed() {
		
		Intent intent = new Intent(context,
				FragmentDisplayActivity.class);
		intent.putExtra("selected", "worklist");
		intent.putExtra("flag", 1);
		startActivity(intent);

		

	}

}
