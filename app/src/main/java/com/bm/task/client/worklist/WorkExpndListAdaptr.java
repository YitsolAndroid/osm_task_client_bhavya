/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;


import com.bm.task.client.domain.ServerDomain;
import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.fragment.FragmentForWorkList;
import com.bm.task.client.restWS.server.GetChangeStatusServer;
import com.bm.task.client.restWS.server.GetOrderServerXML;
import com.bm.task.client.restWS.server.GetProcessHistoryServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.utils.ServerUrl;
import com.bm.task.client.xml.WorkListDomain;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

@SuppressLint("DefaultLocale")
public class WorkExpndListAdaptr extends BaseExpandableListAdapter implements
		OnClickListener {

	private Context con;

	private ExpandableListView workList;

	private ArrayList<ArrayList<WorkListDomain>> childList;
	public static List<WorkListDomain> taskList = new ArrayList<WorkListDomain>();
	private List<WorkListDomain> lst;

	public static String user;
	public static String orderState;
	public static String task;
	public static String orderSource;
	public static String priority;
	public static String ref;
	public static String process;
	public static String type;

	private String childOrderId;

	private List<ServerDomain> processlst;

	protected String encodeData;

	protected ConnectionDetector connectionDetector;

	public static String id;

	public WorkExpndListAdaptr(Context con, List<WorkListDomain> lst,
			ArrayList<ArrayList<WorkListDomain>> childList,
			ExpandableListView workList, List<ServerDomain> processlst) {

		this.con = con;
		this.lst = lst;
		this.childList = childList;
		this.workList = workList;
		this.processlst = processlst;

	}

	@Override
	public int getGroupCount() {
		return lst.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return childList.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return lst.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return childList.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getGroupView(final int position, final boolean isExpanded,
			View v, final ViewGroup parent) {
		if (v == null) {
			LayoutInflater in = (LayoutInflater) con
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = in.inflate(R.layout.txt_worklist, null);

		}
		ViewGroup root = (ViewGroup) v.findViewById(R.id.ll_worklist);
		new FontType(con, root);
		TextView txt_id = (TextView) v.findViewById(R.id.txt_id);
		TextView txt_orderState = (TextView) v
				.findViewById(R.id.txt_orderState);
		TextView txt_type = (TextView) v.findViewById(R.id.txt_type);
		TextView txt_task = (TextView) v.findViewById(R.id.txt_orderTask);
		TextView txt_user = (TextView) v.findViewById(R.id.txt_user);
		TextView txt_state = (TextView) v.findViewById(R.id.txt_state);
		TextView txt_process = (TextView) v.findViewById(R.id.txt_orderProcess);
		ImageView img_indicator = (ImageView) v
				.findViewById(R.id.img_indicator);
		ImageView img_attach = (ImageView) v
				.findViewById(R.id.img_attach);
		
		System.out.println("My remars"+lst.get(position).getNumRemarks());
		
		if(lst.get(position).getNumRemarks().equalsIgnoreCase("0"))
		{
			img_attach.setVisibility(View.GONE);
		}
		else
		{
			img_attach.setVisibility(View.VISIBLE);
		}
		
		img_attach.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				id = lst.get(position).getSeqIds();
				user = lst.get(position).getUser();
				task = lst.get(position).getTask();
				orderSource = lst.get(position).getSource();
				priority = lst.get(position).getPriority();
				ref = lst.get(position).getRef_no();
				process = lst.get(position).getProcessStatus();
				String currentOrderState = lst.get(position)
						.getCurrentOrderState();
				String[] state = currentOrderState.split("\\.");
				orderState = state[state.length - 1];
				type = lst.get(position).getType();
				
				connectionDetector = new ConnectionDetector(con);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							con, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{
				
				Intent in=new Intent(con,FragmentForWorkList.class);
				in.putExtra("selected", "displayremark");
				in.putExtra("flag", 1);
				con.startActivity(in);
				}
				
			}
		});
		

		String orderTask = lst.get(position).getTask();
		String[] arrayTask = orderTask.split("_");
		orderTask = arrayTask[0];

		txt_id.setText("Id : " + lst.get(position).getSeqIds());
		txt_task.setText("" + orderTask);
		txt_user.setText("" + lst.get(position).getUser());
		txt_type.setText("Type : " + lst.get(position).getType());

		for (int i = 0; i < processlst.size(); i++) {
			if (processlst.get(i).getValue()
					.equalsIgnoreCase(lst.get(position).getSeqIds())) {
				txt_process.setText("Process : " + processlst.get(i).getKey());
			}
		}

		txt_id.setTextColor(Color.BLACK);
		txt_task.setTextColor(Color.BLACK);
		txt_type.setTextColor(Color.BLACK);
		txt_user.setTextColor(Color.BLACK);
		txt_state.setTextColor(Color.BLACK);
		txt_process.setTextColor(Color.BLACK);
		txt_orderState.setTextColor(Color.BLACK);

		String currentOrderState = lst.get(position).getCurrentOrderState();
		String status = lst.get(position).getOrderState();
		status = status.substring(0, 1).toUpperCase() + status.substring(1);
		String[] state = currentOrderState.split("\\.");
		String currentState = state[state.length - 1];
		currentState = currentState.substring(0, 1).toUpperCase()
				+ currentState.substring(1);
		currentState = currentState.replaceAll("_", "");

		txt_state.setText("OrderState : " + currentState);
		txt_orderState.setText("State : " + status);

		if (currentState.equalsIgnoreCase("Suspended")
				|| currentState.equalsIgnoreCase("Cancelled")
				|| currentState.equalsIgnoreCase("Cancelling")
				|| currentState.equalsIgnoreCase("Failed")) {
			txt_state.setTextColor(Color.RED);
		} else {
			txt_state.setTextColor(Color.BLACK);
		}

		if (position % 2 == 0) {

			v.setBackgroundColor(Color.parseColor("#ffffff"));
		} else {
			v.setBackgroundColor(Color.parseColor("#e2e2e2"));
		}

		img_indicator.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isExpanded)
					((ExpandableListView) parent).collapseGroup(position);
				else
					((ExpandableListView) parent).expandGroup(position, true);

			}
		});

		/**
		 * Click on Parent List
		 */
		workList.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int position, long pos) {
				
				id = lst.get(position).getSeqIds();
				user = lst.get(position).getUser();
				task = lst.get(position).getTask();
				orderSource = lst.get(position).getSource();
				priority = lst.get(position).getPriority();
				ref = lst.get(position).getRef_no();
				process = lst.get(position).getProcessStatus();
				String currentOrderState = lst.get(position)
						.getCurrentOrderState();
				String[] state = currentOrderState.split("\\.");
				orderState = state[state.length - 1];
				type = lst.get(position).getType();

				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
						.permitAll().build();
				StrictMode.setThreadPolicy(policy);

				taskList.clear();
				connectionDetector = new ConnectionDetector(con);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							con, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{

				if (WorkListActivity.selected.equalsIgnoreCase("Editor")) {
					
								
					Encryption encryption = new Encryption();
					try {
						String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetOrderServerXML(con, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
							lst.get(position).getSeqIds(), CheckConnectivity.server,CheckConnectivity.port,"true",WorkExpndListAdaptr.task,encoded,WorkExpndListAdaptr.user).
							execute(ServerUrl.serverUrlXml + "getOrder");
						
						
						
					
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					

				
				}
				if (WorkListActivity.selected.equalsIgnoreCase("Change Status")) {
					

					Encryption encryption = new Encryption();
					try {
						String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetChangeStatusServer(con, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, lst.get(position).getSeqIds(),
							CheckConnectivity.server, CheckConnectivity.port, lst.get(position).getTask(),encoded).execute(ServerUrl.serverUrlXml + "listStatesNStatuses");
					
					
					
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
				
				}

				if (WorkListActivity.selected
						.equalsIgnoreCase("Process History")) {
					

					Encryption encryption = new Encryption();
					try {
						String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetProcessHistoryServer(con, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
							lst.get(position).getSeqIds(),CheckConnectivity.server,CheckConnectivity.port,lst.get(position).getRef_no(),encoded).execute(ServerUrl.serverUrlXml + "getOrderProcessHistory");
					
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				
				if (WorkListActivity.selected
						.equalsIgnoreCase("Add Remark")) {
					Intent in = new Intent(con, FragmentForWorkList.class);
					in.putExtra("selected", "addremark");
					in.putExtra("flag", 1);
					con.startActivity(in); 
				}
				}

				return true;

			}

		});

		return v;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getChildView(final int position, int childPosition,
			boolean isLastChild, View v, ViewGroup parent) {
		if (v == null) {
			LayoutInflater in = (LayoutInflater) con
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = in.inflate(R.layout.txt_child, null);
		}

		ViewGroup root = (ViewGroup) v.findViewById(R.id.ll_child);
		new FontType(con, root);
		childOrderId = lst.get(position).getSeqIds();
		user = lst.get(position).getUser();
		task = lst.get(position).getTask();
		orderSource = lst.get(position).getSource();
		priority = lst.get(position).getPriority();
		ref = lst.get(position).getRef_no();
		process = lst.get(position).getProcessStatus();
		String currentOrderState = lst.get(position).getCurrentOrderState();
		String[] state = currentOrderState.split("\\.");
		orderState = state[state.length - 1];
		type = lst.get(position).getType();
		TextView tvSuspend = (TextView) v.findViewById(R.id.txt_suspend);
		TextView tvCancel = (TextView) v.findViewById(R.id.txt_cancel);
		TextView tvResume = (TextView) v.findViewById(R.id.txt_resume);
		TextView tvAbort = (TextView) v.findViewById(R.id.txt_abort);
		tvSuspend.setText(""
				+ childList.get(position).get(childPosition).getSuspend());
		tvCancel.setText(""
				+ childList.get(position).get(childPosition).getCancel());
		tvResume.setText(""
				+ childList.get(position).get(childPosition).getResume());
		tvAbort.setText(""
				+ childList.get(position).get(childPosition).getAbort());

		tvSuspend.setOnClickListener(WorkExpndListAdaptr.this);
		tvCancel.setOnClickListener(WorkExpndListAdaptr.this);
		tvResume.setOnClickListener(WorkExpndListAdaptr.this);
		tvAbort.setOnClickListener(WorkExpndListAdaptr.this);

		return v;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.txt_suspend:
			gotoChangeOrders("Suspend Order");
			break;

		case R.id.txt_cancel:
			gotoChangeOrders("Cancel Order");

			break;

		case R.id.txt_resume:
			gotoChangeOrders("Resume Order");

			break;

		case R.id.txt_abort:
			gotoChangeOrders("Abort Order");

			break;

		default:
			break;
		}

	}

	/**
	 * 
	 * for Changing OrderState
	 * 
	 * @param order
	 */

	private void gotoChangeOrders(String order) {
		
		connectionDetector = new ConnectionDetector(con);
		if (!connectionDetector.isConnectingToInternet()) {
			CommonPopUp commonPopUp = new CommonPopUp(
					con, "Please activate internet...");
			commonPopUp.setCanceledOnTouchOutside(true);
			commonPopUp.show();
		}else{

		Intent in = new Intent(con, PopupOrders.class);
		in.putExtra("orderId", childOrderId);
		in.putExtra("order", order);
		con.startActivity(in);
		}

	}

}
