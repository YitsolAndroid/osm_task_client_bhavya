/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;


import com.bm.task.client.restWS.server.LoginServer;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class LoginActivity extends Activity {

	private EditText edtUserName;
	private EditText edtPassword;
	private Button btnLogin;
	private LinearLayout ll_settings;
	private ConnectionDetector connectionDetector;
	protected Encryption encryption;
	protected static final String PREFRENCES_NAME = "Server";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login);
		edtUserName = (EditText) findViewById(R.id.edt_username);
		edtPassword = (EditText) findViewById(R.id.edt_password);
		
		// Button id initialisation
		btnLogin = (Button) findViewById(R.id.btn_login);

		ll_settings = (LinearLayout) findViewById(R.id.ll_settings);

		
		 ViewGroup root = (ViewGroup) findViewById(R.id.ll_login); 
		 new FontType(getApplicationContext(), root);
		 

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		connectionDetector = new ConnectionDetector(getApplicationContext());

		btnLogin.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				
				SharedPreferences settings = getSharedPreferences(PREFRENCES_NAME, 0);		
				CheckConnectivity.server = settings.getString("ip", "");
				CheckConnectivity.port = settings.getString("port", "");
				

				String userName = edtUserName.getText().toString();
				String password = edtPassword.getText().toString();
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							LoginActivity.this, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				} else if (userName.equalsIgnoreCase("")
							&& password.equalsIgnoreCase("")) {
						CommonPopUp commonPopUp = new CommonPopUp(
								LoginActivity.this,
								"Please Enter UserName and Password");
						commonPopUp.setCanceledOnTouchOutside(true);
						commonPopUp.show();

					} else {
					
						boolean connectivity;
						encryption = new Encryption();
						try {
							CheckConnectivity checkConnectivity = new CheckConnectivity();
							connectivity = checkConnectivity.Connectivity();

							if (connectivity == true) {
								
								String encoded = encryption.encrypt(userName + ":"
										+ password);

								new LoginServer(LoginActivity.this, encoded,userName,password)
										.execute(ServerUrl.serverUrlXml + "login");
								
							} else {

								Intent in = new Intent(LoginActivity.this,
										PopUpForIP.class);
								startActivity(in);

							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					

					
				}

			}
		});

		ll_settings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this, ChangeIP.class);
				startActivity(intent);
			}
		});

	}

	public void onBackPressed() {

	}
}
