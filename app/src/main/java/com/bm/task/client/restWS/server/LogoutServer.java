package com.bm.task.client.restWS.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.osm_xmlapi.R;

@SuppressLint("ClickableViewAccessibility")
public class LogoutServer extends AsyncTask<String, Void, String> {

	ProgressDialog progressDialog;
	String result;
	Context context;
	JSONObject json;
	private String encoded;
	private String publicIP;
	private String port;
	

	public LogoutServer(Context context,String encoded,String publicIP,String port) {

		this.encoded = encoded;
		this.context = context;
		this.publicIP =publicIP;
		this.port = port;

	}

	protected void onPreExecute() {

		progressDialog = new ProgressDialog(context, R.style.MyTheme);
		progressDialog.setCancelable(false);
		progressDialog
				.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		progressDialog.show();

	}

	@Override
	protected String doInBackground(String... urls) {
		try {

			System.out.println(":::::::::::Coming Here :::::::::::::");
			System.out.println(":::::::::::"+encoded+" :::::::::::::");
			// make web service connection
			HttpPost request = new HttpPost(urls[0]);
			request.setHeader("Auth", encoded);
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");
			request.setHeader("Accept-Encoding", "gzip");
			// Build JSON string
			JSONStringer TestApp = new JSONStringer().object()
					.key("publicIP").value(publicIP)
					.key("port").value(port)
					.endObject();
					
			StringEntity entity = new StringEntity(TestApp.toString());

			request.setEntity(entity);

			// Log.d("****Parameter Input****", "Testing:" + TestApp);
			StringBuilder sb = new StringBuilder();
			// Send request to WCF service
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);

			if(response.getStatusLine().toString().equalsIgnoreCase("HTTP/1.1 200 OK"))
			{
				// Get the status of web service
				BufferedReader br = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				// print status in log
				String line = br.readLine();
				sb.append(line);
				result = sb.toString();
			}
			else
			{
				
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", response.getStatusLine().toString());
				result = jsonObject.toString();
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public void onPostExecute(String result) {
		
		progressDialog.dismiss();

		System.out.println("*******----"+result);
	}
}

