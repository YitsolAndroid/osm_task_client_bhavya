/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.bm.task.client.query.GetOrderQueryActivity;
import com.bm.task.client.utils.EditNodeDialog;
import com.bm.task.client.utils.EditNodeListner;
import com.bm.task.client.xml.WorkListDomain;
import com.example.osm_xmlapi.R;

public class GetTestAdapter extends BaseAdapter implements EditNodeListner {

	private ViewHolder holder;
	private LayoutInflater inflator;

	private Context con;
	private List<WorkListDomain> lst;
	private WorkListDomain model;

	private int heading;
	private GetOrderActivity getOrderActivity;

	public GetTestAdapter(Context con, List<WorkListDomain> lst, GetOrderActivity getOrderActivity) {

		this.con = con;
		this.lst = lst;
		this.getOrderActivity= getOrderActivity;
		inflator = (LayoutInflater) con
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	

	@Override
	public int getCount() {
		return lst.size();

	}

	@Override
	public Object getItem(int position) {
		return lst.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint({ "InflateParams", "ResourceAsColor" })
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		if (v == null) {

			holder = new ViewHolder();
			v = inflator.inflate(R.layout.txt_test_order, null);
			holder.edt_test = (TextView) v.findViewById(R.id.txt_value);
			holder.txt_test = (TextView) v.findViewById(R.id.txt_test);
			holder.editbutt = (Button) v.findViewById(R.id.editbutt);

			v.setTag(holder);

		} else {
			holder = (ViewHolder) v.getTag();
		}

		model = (WorkListDomain) getItem(position);

		holder.editbutt.setTag(model);
		holder.txt_test.setTag(model);
		holder.edt_test.setTag(model);

		if (model.getNodeValue().equalsIgnoreCase("")) {

			heading = heading + 1;
			holder.edt_test.setVisibility(View.GONE);
			holder.editbutt.setVisibility(View.GONE);
			holder.txt_test.setText(model.getNodeName());
			holder.txt_test.setTextColor(Color.BLACK);
			holder.edt_test.setText(model.getNodeValue());
		} else {
			heading = 0;
			holder.edt_test.setVisibility(View.VISIBLE);
			holder.editbutt.setVisibility(View.VISIBLE);
			holder.txt_test.setText(model.getNodeName());
			holder.txt_test.setTextColor(Color.BLACK);
			holder.edt_test.setText(model.getNodeValue());
		}

		holder.editbutt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				model = (WorkListDomain) v.getTag();

				showtheaddnotedialog(model);

			}
		});

		return v;

	}
	
	/**
	 * Opening of Dialog for Edit the Values
	 * @param model
	 */
	protected void showtheaddnotedialog(WorkListDomain model) {
		EditNodeDialog addnote = new EditNodeDialog(con, model);

		addnote.setCanceledOnTouchOutside(false);

		addnote.registerlistner(this);

		addnote.show();
	}

	private class ViewHolder {
		TextView txt_test;
		TextView edt_test;
		Button editbutt;
	}

	@Override
	public void getEditNodeText(String editNode) {
		notifyDataSetChanged();
		getOrderActivity.getList(lst);
	}

	

}
