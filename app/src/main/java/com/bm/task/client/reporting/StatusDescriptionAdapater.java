/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.reporting;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bm.task.client.utils.FontType;
import com.example.osm_xmlapi.R;

public class StatusDescriptionAdapater extends BaseAdapter {
	
	Context con;
	public static List<StatusDomain> lst;

	@SuppressWarnings("static-access")
	public StatusDescriptionAdapater(Context con,List<StatusDomain> lst){
		
		this.con=con;
		this.lst=lst;
		
	}

	@Override
	public int getCount() {
		return lst.size();
		
	}

	@Override
	public Object getItem(int position) {
		return lst.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		if(v==null)
		{
			LayoutInflater in=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v=in.inflate(R.layout.txt_lstdescription, null);
			
		}
		
		ViewGroup root = (ViewGroup) v.findViewById(R.id.ll_orders);
		new FontType(con, root);
		TextView txtStatus=(TextView)v.findViewById(R.id.txt_name);
		TextView txtcount=(TextView)v.findViewById(R.id.txt_count);
		
		txtStatus.setText(""+lst.get(position).getStatusName());
		txtcount.setText(""+lst.get(position).getStatusCount());
		
	
		
		if (position % 2 == 1) {

			v.setBackgroundColor(Color.parseColor("#1E90FF"));
		} else {
			v.setBackgroundColor(Color.parseColor("#87CEFA"));
		}
		
	return v;
	}
	}
