package com.bm.task.client.worklist;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bm.task.client.utils.FontType;
import com.example.osm_xmlapi.R;

public class HistDescAdapter extends BaseAdapter {


	private Context con;
	private List<ProcessHistoryDomain> lst;
	private int days;
	private int hrs;
	private int mins;
	private int secs;
	
	

	public HistDescAdapter(Context con,
			List<ProcessHistoryDomain> lst) {
		this.con=con;
		this.lst=lst;
	}


	@Override
	public int getCount() {
		return lst.size();

	}

	@Override
	public Object getItem(int position) {
		return lst.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		if(v==null)
		{
			LayoutInflater in=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v=in.inflate(R.layout.txt_hist_description, null);
			
		}
		
		ViewGroup root = (ViewGroup) v.findViewById(R.id.ll_txt_histDesc);
		new FontType(con, root);
		TextView txtActulDuration=(TextView)v.findViewById(R.id.txt_ActualDuration);
		TextView txtExpected=(TextView)v.findViewById(R.id.txt_expected);
		TextView txtStartDate=(TextView)v.findViewById(R.id.txt_started);
		TextView txtCompleted=(TextView)v.findViewById(R.id.txt_completed);
		TextView txtStatus=(TextView)v.findViewById(R.id.txt_status);
		TextView txtState=(TextView)v.findViewById(R.id.txt_state);
		TextView txtUser=(TextView)v.findViewById(R.id.txt_user);
		
		
		txtExpected.setTextColor(Color.BLACK);
		txtCompleted.setTextColor(Color.BLACK);
		txtStatus.setTextColor(Color.BLACK);
		txtState.setTextColor(Color.BLACK);
		txtStartDate.setTextColor(Color.BLACK);
		txtUser.setTextColor(Color.BLACK);
		txtActulDuration.setTextColor(Color.BLACK);
		
		String startDate = "--";
		String completed = "--";
		String expectedDate = "--";
		int actualDuration=0;

		
		try {
			if (lst.get(position).getCompleted() != "")
				completed = getFormattedDateTime(lst.get(position).getCompleted());
			if (lst.get(position).getStarted() != "")
				startDate = getFormattedDateTime(lst.get(position).getStarted());
			if (lst.get(position).getExpectedCompletionDate() != "")
				expectedDate = getFormattedDateTime(lst.get(position).getExpectedCompletionDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		txtExpected.setText("Expected Date Completion : "+expectedDate);
		txtStartDate.setText("Started : "+startDate);
		txtCompleted.setText("Completed : "+completed);
		txtState.setText("State : "+lst.get(position).getState());
		txtUser.setText("User : "+lst.get(position).getUser());
		actualDuration=Integer.parseInt(lst.get(position).getActualDuration());
		getSecondConverter(actualDuration);
		txtActulDuration.setText("Actual Duration : "+days+"day(s) "+hrs+"hr(s) "+mins+"min(s) "+secs+"sec(s)");
		
		
		
		if(lst.get(position).getStatus().equalsIgnoreCase("n/a"))
		{
			txtStatus.setText("Status : "+"--");
		}
		else
		{
			txtStatus.setText("Status : "+lst.get(position).getStatus());
		}
		
		if (position % 2 == 1) 
		{
			
			v.setBackgroundColor(Color.parseColor("#e2e2e2"));
		} 
		else
		{
			v.setBackgroundColor(Color.parseColor("#ffffff"));
		}
		
	
		
		return v;

	}
	

	@SuppressLint("SimpleDateFormat")
	/**
	 * Date for Custom Display
	 * @param formateDate
	 * @return
	 * @throws ParseException
	 */
	public String getFormattedDateTime(String formateDate)
			throws ParseException {

		System.out.println(formateDate);

		String[] dateList = formateDate.split("T");
		String date = dateList[0] + " " + dateList[1];
		dateList = date.split("IS");
		date = dateList[0];

		try {
			DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DateFormat outputFormat = new SimpleDateFormat(
					"dd/MM/yyyy\tKK:mm:ss a");
			date = outputFormat.format(inputFormat.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return date;
	}
	
	/**
	 * converting Seconds to Day hours minutes and seconds
	 * @param actualDuration
	 */
	
	private void getSecondConverter(int actualDuration) {
		days = actualDuration/86400;
		actualDuration = actualDuration%86400;
		hrs = actualDuration/3600;
		actualDuration = actualDuration%3600;
		mins = actualDuration/60;
		actualDuration = actualDuration%60;
		secs = actualDuration;
	

	}
}
