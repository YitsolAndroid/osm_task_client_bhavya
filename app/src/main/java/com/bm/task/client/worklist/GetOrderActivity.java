
 /**
  *  Copyright(C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
  */
 

package com.bm.task.client.worklist;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.fragment.FragmentForWorkList;
import com.bm.task.client.restWS.server.GetOrderServerXML;
import com.bm.task.client.restWS.server.GetProcessStatus;
import com.bm.task.client.restWS.server.UpdateOrderServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.utils.NextActivity;
import com.bm.task.client.utils.ServerUrl;
import com.bm.task.client.xml.WorkListDomain;

import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.xml.XMLParser;

 /**
 * Getting task details and Status
 * 
 * @author Bhavya
 * 
 */
public class GetOrderActivity extends Fragment {

	private XMLParser xmlParser;

	private TextView txtOrderId;
	private TextView txtOrderType;
	private TextView txtOrderSource;
	private TextView txtTask;

	private TextView edtRefNo;

	private ListView lstTest;

	private Spinner spnStatus;
	private Spinner spnPriority;

	private Button btnUpdate;
	private Button btnCancel;


	private List<WorkListDomain> newEditedList = new ArrayList<WorkListDomain>();
	private List<WorkListDomain> lstForUpdate = new ArrayList<WorkListDomain>();
	private String[] priorityList = { "1", "2", "3", "4", "5", "6", "7", "8",
			"9" };
	private WorkListDomain workListDomain;

	private String rootUpdate;
	private String status;
	private String id;

	private Context context;


	private ViewGroup root;

	protected ConnectionDetector connectionDetector;
	
	public GetOrderActivity(Context con,String id)
	{
		this.id=id;
		this.context = con;
	}

	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.get_order, container, false);
	        
	        
	        root = (ViewGroup)rootView.findViewById(R.id.ll_editor);
	        new FontType(context, root);

		txtOrderId = (TextView) rootView.findViewById(R.id.txt_OrderId);
		txtOrderType = (TextView) rootView.findViewById(R.id.txt_OrderType);
		txtOrderSource = (TextView) rootView.findViewById(R.id.txt_OrderSource);
		txtTask = (TextView) rootView.findViewById(R.id.txt_task);

		edtRefNo = (TextView) rootView.findViewById(R.id.edt_refNo);

		spnPriority = (Spinner) rootView.findViewById(R.id.spnr_priority);
		spnStatus = (Spinner) rootView.findViewById(R.id.spnr_Status);

		lstTest = (ListView) rootView.findViewById(R.id.lst_Test);

		btnUpdate = (Button) rootView.findViewById(R.id.btn_update);
		btnCancel = (Button) rootView.findViewById(R.id.btn_Cancel);


		txtOrderId.setText("ID : " + id);
		txtOrderSource.setText("Source : " + WorkExpndListAdaptr.orderSource);
		txtOrderType.setText("Type : " + WorkExpndListAdaptr.type);
		// txtProcess.setText("Process : " + WorkExpndListAdaptr.process);
		txtTask.setText("Task : " + WorkExpndListAdaptr.task);
		int count = Integer.parseInt(WorkExpndListAdaptr.priority);
		spnPriority.setAdapter(new SpnAdapter(context,
				priorityList));
		spnPriority.setSelection(count - 1);
		edtRefNo.setText("Ref #. : " + WorkExpndListAdaptr.ref);

		lstTest.setAdapter(new GetTestAdapter(context,
				WorkExpndListAdaptr.taskList,GetOrderActivity.this));

		Encryption encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(NextActivity.userName + ":"
					+ NextActivity.password);
			new GetProcessStatus(context, spnStatus, encoded)
					.execute(ServerUrl.serverUrl + "getProcess?orderId=" + id
							+ "&task=" + WorkExpndListAdaptr.task);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		spnStatus.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				status = parent.getAdapter().getItem(position).toString();
				System.out.println("Spn status For Getorder is= " + status);
				Toast.makeText(context, status,
						Toast.LENGTH_SHORT).show();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		/*** END Getting Status For Complete Order ***/

		btnUpdate.setOnClickListener(new OnClickListener() {

			private String xml = "";
			private String[] updateArray;

			@Override
			public void onClick(View v) {
				try {
					connectionDetector = new ConnectionDetector(context);
					if (!connectionDetector.isConnectingToInternet()) {
						CommonPopUp commonPopUp = new CommonPopUp(
								context, "Please activate internet...");
						commonPopUp.setCanceledOnTouchOutside(true);
						commonPopUp.show();
					}else{

					for (int i = 0; i < newEditedList.size(); i++) {

						if (newEditedList.get(i).getNodeValue()
								.equalsIgnoreCase("")) {
						} else {
							String nodeName = newEditedList.get(i)
									.getNodeName();
							String nodeValue = newEditedList.get(i)
									.getNodeValue();
							workListDomain = new WorkListDomain();
							workListDomain.setNodeName(nodeName);
							workListDomain.setNodeValue(nodeValue);
							lstForUpdate.add(workListDomain);
							Log.e(GetOrderActivity.class.getSimpleName(), "in button update click=" + newEditedList.get(i).getNodeValue());

						}
					}
					Document doc;
					xmlParser = new XMLParser();

					doc = xmlParser.getDomElement(GetOrderServerXML.xml);
					getData(null, doc.getDocumentElement());

					
					System.out .println("IN GETORDER :::  " +
					GetOrderServerXML.xml);
					
					newEditedList.clear();
					 

					TransformerFactory tFact = TransformerFactory.newInstance();
					Transformer trans = tFact.newTransformer();
					StringWriter writer = new StringWriter();
					StreamResult result = new StreamResult(writer);
					DOMSource source = new DOMSource(doc);
					trans.transform(source, result);
					rootUpdate = writer.toString();

					
				
					 

					String[] strStart = rootUpdate.split("</xmlns:xsi>");
					rootUpdate = "<_root>" + strStart[1];
					String[] strEnd = rootUpdate.split("</_root>");
					rootUpdate = strEnd[0] + "</_root>";

					xml = rootUpdate;
					System.out.println("Updated ***********" + xml);

					Encryption encryption = new Encryption();
					try {
						String encoded = encryption
								.encrypt(NextActivity.userName + ":"
										+ NextActivity.password);

						new UpdateOrderServer(context, id, xml,
								NextActivity.userName, NextActivity.password,
								status,WorkExpndListAdaptr.task, encoded,"worklist").execute(ServerUrl.serverUrlXml
								+ "updateOrder");

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				} 
				}catch (TransformerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				connectionDetector = new ConnectionDetector(context);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							context, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{

				Intent in = new Intent(context,
						FragmentForWorkList.class);
				in.putExtra("selected", "");
				in.putExtra("flag", 0);
				context.startActivity(in);
				}

			}
		});

		
		return rootView;
	}

	/**
	 * Editing node value in the XML and passing XML as XMLAPI in task Client
	 * 
	 * @param parentNode
	 * @param node
	 */
	public void getData(Node parentNode, Node node) {

		switch (node.getNodeType()) {
		case Node.ELEMENT_NODE: {

			if (node.hasChildNodes()) {
				NodeList list = node.getChildNodes();
				int size = list.getLength();
				for (int index = 0; index < size; index++) {
					getData(node, list.item(index));
				}
			}

			break;
		}

		case Node.TEXT_NODE: {
			String data = node.getNodeValue();

			if (data.trim().length() > 0) {
				node.getNodeValue();
				for (int i = 0; i < lstForUpdate.size(); i++) {
					
					System.out.println(parentNode.getNodeName()+"====="+lstForUpdate.get(i).getNodeName());

					if (parentNode.getNodeName().equalsIgnoreCase(
							lstForUpdate.get(i).getNodeName())) {

						node.setTextContent(lstForUpdate.get(i).getNodeValue());
					}
				}
			}
			break;
		}

		}
	}

	/**
	 * Edited Lst For Update values
	 * 
	 * @param lst
	 */
	public void getList(List<WorkListDomain> lst) {
		this.newEditedList = lst;
		Log.e(GetOrderActivity.class.getSimpleName(), "Size=" + lst.size());
		
		for(int i=0;i<lst.size();i++)
		{
			Log.e(GetOrderActivity.class.getSimpleName(), "value=" + lst.get(i).getNodeValue());
		}

	}

	/**
	 * To Disable Back Button of Device
	 */
public void onBackPressed() {
		
		Intent intent = new Intent(context,
				FragmentDisplayActivity.class);
		intent.putExtra("selected", "worklist");
		intent.putExtra("flag", 1);
		startActivity(intent);

		

	}

}
/*updateArray = rootUpdate.split("Data"); 
rootUpdate = updateArray[1]; 
updateArray = rootUpdate.split("<osmc:");
String root = "<_root>";
for (int i = 0; i < updateArray.length; i++) 
{ 
	if (i == 0 || i == 1) { } 
else
 { 
	root = root + "<" + updateArray[i]; 
	} 
	} 
updateArray = root.split("</osmc:"); 
for (int i = 0; i < updateArray.length; i++) {

 if (i == updateArray.length - 1) {
 
 } else if (i == 0) { xml = xml + updateArray[i]; } else {
 xml = xml + "</" + updateArray[i]; }
 
 } xml = xml + "</_root>";*/