package com.bm.task.client.restWS.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;


import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.fragment.FragmentForQuery;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.xml.WorkListDomain;

import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;
import src.main.java.com.bm.task.client.xml.XMLParser;

@SuppressLint("ClickableViewAccessibility")
public class GetFindOrder extends AsyncTask<String, Void, String> {

	ProgressDialog progressDialog;
	String result;
	Context context;
	JSONObject json;
	private String userName;
	private String password;
	public static String xml;
	private String encoded;
	private String orderId;
	private String server;
	private String port;
	private String user;
	private String task;
	private String source;
	private String type;
	private String orderState;
	private Object executionmode;
	private Object nameSpace;
	private Object reference;
	private Object state;
	private String createDateFrom;
	private String createDateTo;
	private String status;
	public static List<WorkListDomain> querylist = new ArrayList<WorkListDomain>();
	
	private static final String KEY_ITEM_Error = "error";
	private static final String KEY_ITEM_Error1 = "FindOrder.Error";
	private static final String Error = "Error";


	public GetFindOrder(Context context, String userName, String password,
			String orderId, String reference, String server, String port,
			String user, String task, String source, String type,
			String orderState, String executionmode, String nameSpace,
			String state, String status, String createDateFrom,
			String createDateTo, String encoded) {

		this.userName = userName;
		this.password = password;
		this.orderId = orderId;
		this.server = server;
		this.port = port;
		this.user = user;
		this.task = task;
		this.source = source;
		this.type = type;
		this.orderState = orderState;
		this.executionmode = executionmode;
		this.nameSpace = nameSpace;
		this.reference = reference;
		this.state = state;
		this.status =status;
		this.createDateFrom = createDateFrom;
		this.createDateTo = createDateTo;
		this.encoded = encoded;
		this.context = context;

	}

	protected void onPreExecute() {

		progressDialog = new ProgressDialog(context, R.style.MyTheme);
		progressDialog.setCancelable(false);
		progressDialog
				.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		progressDialog.show();

	}

	@Override
	protected String doInBackground(String... urls) {
		try {
			CheckConnectivity checkConnectivity = new CheckConnectivity();
			if(checkConnectivity.Connectivity() == true){
			// make web service connection
			HttpPost request = new HttpPost(urls[0]);
			request.setHeader("Auth", encoded);
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");
			request.setHeader("Accept-Encoding", "gzip");
			System.out.println(FragmentDisplayActivity.userName + FragmentDisplayActivity.password + encoded);
			// Build JSON string
			JSONStringer TestApp = new JSONStringer().object()
					.key("username").value(userName)
					.key("password").value(password)
					.key("inputString").value("")
					.key("executionmode").value(executionmode)
					.key("namespace").value(nameSpace)
					.key("orderId").value(orderId)
					.key("orderSource").value(source)
					.key("orderState").value(orderState)
					.key("orderType").value(type)
					.key("user").value(user)
					.key("task").value(task)
					.key("status").value(status)
					.key("state").value(state)
					.key("reference").value(reference)
					.key("createDateFrom").value(createDateFrom)
					.key("createDateTo").value(createDateTo)
					.key("port").value(reference)
					.key("publicIP").value(server)
				.endObject();
			StringEntity entity = new StringEntity(TestApp.toString());
			
			/*.key("username").value(userName)
			.key("password").value(password)
			.key("orderId").value(orderId)
			.key("reference").value(reference)
			.key("publicIP").value(server)
			.key("port").value(port)
			.key("user").value(user)
			.key("task").value(task)
			.key("orderSource").value(source)
			.key("orderType").value(type)
			.key("orderState").value(orderState)
			.key("executionmode").value(executionmode)
			.key("namespace").value(nameSpace)
			.key("state").value(state)
			.key("status").value(status)
			.key("createDateFrom").value(createDateFrom)
			.key("createDateTo").value(createDateTo)*/

			request.setEntity(entity);

			// Log.d("****Parameter Input****", "Testing:" + TestApp);
			StringBuilder sb = new StringBuilder();
			// Send request to WCF service
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);
			
			if(response.getStatusLine().toString().equalsIgnoreCase("HTTP/1.1 200 OK"))
			{
				// Get the status of web service
				BufferedReader br = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				// print status in log
				String line = br.readLine();
				sb.append(line);
				result = sb.toString();
			}
			else
			{
				
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", response.getStatusLine().toString());
				result = jsonObject.toString();
			}
			}
			else
			{JSONObject jsonObject =new JSONObject();
			jsonObject.put("status", "OSM is not connected");
			result = jsonObject.toString();
			}

			
		

		} catch (Exception e) {
			
		}

		return result;
	}

	public void onPostExecute(String result) {

		progressDialog.dismiss();

		try {
			JSONObject json = new JSONObject(result);
			xml = XML.toString(json);
			System.out.println(xml);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(xml.startsWith("<status>")){
			xml = "<error>"+xml+"</error>";
		}
		System.out.println("My XML is ::: ::: "+xml);  
		XMLParser xmlParser = new XMLParser();
		Document doc = xmlParser.getDomElement(xml);
       

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		NodeList error1 = doc.getElementsByTagName(KEY_ITEM_Error1);
		if (error.getLength() >= 1 || error1.getLength() >= 1) {
			if(error.getLength()>=1){
					String desc,code;
					Element e = null;
					
					for (int position = 0; position < error.getLength(); position++) {
						e = (Element) error.item(position);
					}
					
					code = xmlParser.getValue(e, "status");
					desc = xmlParser.getValue(e, "errorMsg");
				
					CommonPopUp commonPopUp = new CommonPopUp(
							context,"\nError Code : "+code +"\nDescription : "+desc	+"\n");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				
			}
			if(error1.getLength()>=1){
			String desc,code;
			Element e = null;
			NodeList nl = doc.getElementsByTagName(Error);
			
			for (int position = 0; position < nl.getLength(); position++) {
				e = (Element) nl.item(position);
			}
			
			desc = xmlParser.getValue(e, "desc");
			code = xmlParser.getValue(e, "code");
		
			CommonPopUp commonPopUp = new CommonPopUp(
					context,"\nError Code : "+code +"\nDescription : "+desc	+"\n");
			commonPopUp.setCanceledOnTouchOutside(true);
			commonPopUp.show();
			}
			
		} 
		else{
	  

		
		Intent intent = new Intent(context, FragmentForQuery.class);
		intent.putExtra("selected", "search");
		intent.putExtra("flag", 1);
		context.startActivity(intent);
		}
		

	}
}
