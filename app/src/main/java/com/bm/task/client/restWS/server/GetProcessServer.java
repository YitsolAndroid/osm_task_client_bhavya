package com.bm.task.client.restWS.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ExpandableListView;


import com.bm.task.client.domain.ServerDomain;
import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.ServerUrl;

import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;
import src.main.java.com.bm.task.client.xml.XMLParser;

@SuppressLint("ClickableViewAccessibility")
public class GetProcessServer extends AsyncTask<String, Void, String> {
	
	private static final String KEY_ITEM_Error = "error";
	private static final String KEY_ITEM_Error1 = "";
	private static final String Error = "Error";
	ProgressDialog progressDialog;
	String result;
	Context context;
	JSONObject json;
	private ServerDomain serverDomain;
	private List<ServerDomain> lst = new ArrayList<ServerDomain>();
	private ExpandableListView workList;
	private String encoded;
	private String xml;
	public GetProcessServer(Context context, ExpandableListView workList, String encoded) {
	this.context=context;
	this.workList =workList;
	this.encoded = encoded;
	}

	protected void onPreExecute() {

		 progressDialog = new ProgressDialog(context, R.style.MyTheme);
		 progressDialog.setCancelable(false); progressDialog
		 .setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		 progressDialog.show();

	}

	@Override
	protected String doInBackground(String... urls) {
		try {

			System.out.println( ":::::::::::Coming Here :::::::::::::");
			// make web service connection
			CheckConnectivity checkConnectivity =new CheckConnectivity();
			if(checkConnectivity.Connectivity()==true){
			
			HttpGet request = new HttpGet(urls[0]);
            request.setHeader("Auth", encoded);
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");
			

		//	Log.d("****Parameter Input****", "Testing:" + TestApp);
			StringBuilder sb = new StringBuilder();
			// Send request to WCF service
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);
			

			if(response.getStatusLine().toString().equalsIgnoreCase("HTTP/1.1 200 OK"))
			{
				// Get the status of web service
				BufferedReader br = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				// print status in log
				String line = br.readLine();
				sb.append(line);
				result = sb.toString();
			}
			else
			{
				
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", response.getStatusLine().toString());
				result = jsonObject.toString();
			}
			}
			else
			{
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", "OSM is not connected");
				result = jsonObject.toString();
			}


		}
			catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public void onPostExecute(String result) {

		progressDialog.dismiss();
		System.out.println("comin here" + result);

		if (result.startsWith("{\"status")) {
			System.out.println("comin here" + result);
		try {
			JSONObject json =new JSONObject(result);
			xml = XML.toString(json);
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
			xml = "<error>" + xml + "</error>";
		
		System.out.println("My XML is ::: ::: " + xml);
		XMLParser xmlParser = new XMLParser();
		Document doc = xmlParser.getDomElement(xml);

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		NodeList error1 = doc.getElementsByTagName(KEY_ITEM_Error1);
		if (error.getLength() >= 1 || error1.getLength() >= 1) {
			if (error.getLength() >= 1) {
				String desc, code;
				Element e = null;

				for (int position = 0; position < error.getLength(); position++) {
					e = (Element) error.item(position);
				}

				code = xmlParser.getValue(e, "status");
				desc = xmlParser.getValue(e, "errorMsg");

				CommonPopUp commonPopUp = new CommonPopUp(context,
						"\nError Code : " + code
								+ "\nDescription : " + desc + "\n");
				commonPopUp.setCanceledOnTouchOutside(true);
				commonPopUp.show();

			}
			if (error1.getLength() >= 1) {
				String desc, code;
				Element e = null;
				NodeList nl = doc.getElementsByTagName(Error);

				for (int position = 0; position < nl.getLength(); position++) {
					e = (Element) nl.item(position);
				}

				desc = xmlParser.getValue(e, "desc");
				code = xmlParser.getValue(e, "code");

				CommonPopUp commonPopUp = new CommonPopUp(context, "\nError Code : " + code + "\nDescription : "
						+ desc + "\n");
				commonPopUp.setCanceledOnTouchOutside(true);
				commonPopUp.show();
			}

		}
		}
		
		else{
		
		int i;
		JSONArray jsonArray;
		try {
			jsonArray = new JSONArray(result);
			for (i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				serverDomain = new ServerDomain();

				serverDomain.setKey(jsonObject.getString("processIdDescription"));
				serverDomain.setValue(jsonObject.getString("orderSeqId"));
				lst.add(serverDomain);
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Encryption encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
			new GetWorkListServer(context, FragmentDisplayActivity.userName,FragmentDisplayActivity.password,
					CheckConnectivity.server,CheckConnectivity.port,workList,lst,encoded).
					execute(ServerUrl.serverUrlXml+"getWorklistOrder");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		
		}	  
	
		
		

	}
}



/*
try {
	JSONObject json =new JSONObject();
	xml = XML.toString(json);
} catch (JSONException e2) {
	// TODO Auto-generated catch block
	e2.printStackTrace();
}
if (xml.startsWith("<status>")) {
	xml = "<error>" + xml + "</error>";
}
System.out.println("My XML is ::: ::: " + xml);
XMLParser xmlParser = new XMLParser();
Document doc = xmlParser.getDomElement(xml);

NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
NodeList error1 = doc.getElementsByTagName(KEY_ITEM_Error1);
if (error.getLength() >= 1 || error1.getLength() >= 1) {
	if (error.getLength() >= 1) {
		String desc, code;
		Element e = null;

		for (int position = 0; position < error.getLength(); position++) {
			e = (Element) error.item(position);
		}

		code = xmlParser.getValue(e, "status");
		desc = xmlParser.getValue(e, "errorMsg");

		CommonPopUp commonPopUp = new CommonPopUp(context,
				"\nError Code : " + code
						+ "\nDescription : " + desc + "\n");
		commonPopUp.setCanceledOnTouchOutside(true);
		commonPopUp.show();

	}
	if (error1.getLength() >= 1) {
		String desc, code;
		Element e = null;
		NodeList nl = doc.getElementsByTagName(Error);

		for (int position = 0; position < nl.getLength(); position++) {
			e = (Element) nl.item(position);
		}

		desc = xmlParser.getValue(e, "desc");
		code = xmlParser.getValue(e, "code");

		CommonPopUp commonPopUp = new CommonPopUp(context, "\nError Code : " + code + "\nDescription : "
				+ desc + "\n");
		commonPopUp.setCanceledOnTouchOutside(true);
		commonPopUp.show();
	}

} else {*/
