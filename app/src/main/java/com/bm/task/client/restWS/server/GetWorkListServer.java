package com.bm.task.client.restWS.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;


import com.bm.task.client.domain.ServerDomain;
import com.bm.task.client.restWS.GetWorkListRWS;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.worklist.WorkExpndListAdaptr;
import com.bm.task.client.worklist.WorkListActivity;
import com.bm.task.client.xml.WorkListDomain;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;
import src.main.java.com.bm.task.client.xml.XMLParser;

@SuppressLint("ClickableViewAccessibility")
public class GetWorkListServer extends AsyncTask<String, Void, String> {
	
	private static final String KEY_ITEM_Error = "error";
	private static final String KEY_ITEM_Error1 = "Worklist.Error";
	private static final String Error = "Error";
	ProgressDialog progressDialog;
	String result;
	Context context;
	JSONObject json;
	private String userName;
	private String password;
	public static String xml;
	private String publicIp;
	private String port;
	
	private List<WorkListDomain> parentList = new ArrayList<WorkListDomain>();
	private ExpandableListView workList;
	private WorkListDomain workListDomain;
	private ArrayList<ArrayList<WorkListDomain>> childlist = new ArrayList<ArrayList<WorkListDomain>>();
	private List<WorkListDomain> searchList = new ArrayList<WorkListDomain>();
	private List<ServerDomain> lst;
	private String encoded;
	protected ConnectionDetector connectionDetector;

	

	public GetWorkListServer(Context context,String userName,String password,String publicIp,String port, 
			ExpandableListView workList, List<ServerDomain> lst, String encoded) {
	this.userName=userName;
	this.password = password;
	this.publicIp=publicIp;
	this.port = port;
	this.context=context;
	this.workList = workList;
	this.lst = lst;
	this.encoded = encoded;
	}

	protected void onPreExecute() {

		 progressDialog = new ProgressDialog(context, R.style.MyTheme);
		 progressDialog.setCancelable(false); progressDialog
		 .setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		 progressDialog.show();

	}

	@Override
	protected String doInBackground(String... urls) {
		try {

			System.out.println( ":::::::::::GEt Worklist Coming Here :::::::::::::");
			
			System.out.println(encoded);
			// make web service connection
			CheckConnectivity checkConnectivity =new CheckConnectivity();
			if(checkConnectivity.Connectivity()==true){
			
			HttpPost request = new HttpPost(urls[0]);
			request.setHeader("Auth",encoded);
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");
			request.setHeader("Accept-Encoding", "gzip");
			// Build JSON string
			JSONStringer TestApp = new JSONStringer().object()
					.key("username").value(userName)
					.key("password").value(password)
					.key("publicIP").value(publicIp)
					.key("port").value(port)
					.endObject();
			
			StringEntity entity = new StringEntity(TestApp.toString());

			request.setEntity(entity);

		//	Log.d("****Parameter Input****", "Testing:" + TestApp);
			StringBuilder sb = new StringBuilder();
			// Send request to WCF service
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);
			
			if(response.getStatusLine().toString().equalsIgnoreCase("HTTP/1.1 200 OK"))
			{
				// Get the status of web service
				BufferedReader br = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				// print status in log
				String line = br.readLine();
				sb.append(line);
				result = sb.toString();
			}
			else
			{
				
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", response.getStatusLine().toString());
				result = jsonObject.toString();
			}


		}
			else{
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status","OSM is not connected");
				result = jsonObject.toString();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		

		return result;
	}

	public void onPostExecute(String result) {

		progressDialog.dismiss();
		       
				try {
					 JSONObject json = new JSONObject(result);
					
					xml = XML.toString(json);
					System.out.println("My XML is ::: ::: "+xml);  
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(xml.startsWith("<status>")){
					xml = "<error>"+xml+"</error>";
				}
				System.out.println("My XML is ::: ::: "+xml);  
				XMLParser xmlParser = new XMLParser();
				Document doc = xmlParser.getDomElement(xml);
		       

				NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
				NodeList error1 = doc.getElementsByTagName(KEY_ITEM_Error1);
				if (error.getLength() >= 1 || error1.getLength() >= 1) {
					if(error.getLength()>=1){
							String desc,code;
							Element e = null;
							
							for (int position = 0; position < error.getLength(); position++) {
								e = (Element) error.item(position);
							}
							
							code = xmlParser.getValue(e, "status");
							desc = xmlParser.getValue(e, "errorMsg");
						
							CommonPopUp commonPopUp = new CommonPopUp(
									context,"\nError Code : "+code +"\nDescription : "+desc	+"\n");
							commonPopUp.setCanceledOnTouchOutside(true);
							commonPopUp.show();
						
					}
					if(error1.getLength()>=1){
					String desc,code;
					Element e = null;
					NodeList nl = doc.getElementsByTagName(Error);
					
					for (int position = 0; position < nl.getLength(); position++) {
						e = (Element) nl.item(position);
					}
					
					desc = xmlParser.getValue(e, "desc");
					code = xmlParser.getValue(e, "code");
				
					CommonPopUp commonPopUp = new CommonPopUp(
							context,"\nError Code : "+code +"\nDescription : "+desc	+"\n");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
					}
					
				}
				else{
				GetWorkListRWS getWorkListWS = new GetWorkListRWS(xml);
				parentList=getWorkListWS.getWorkList();
				
				for(int j=0;j<parentList.size();j++)
				{
					ArrayList<WorkListDomain> mylst=new ArrayList<WorkListDomain>();
					workListDomain=new WorkListDomain();
					workListDomain.setSuspend("Suspend Order");
					workListDomain.setResume("Resume Order");
					workListDomain.setAbort("Abort Order");
					workListDomain.setCancel("Cancel Order");
					mylst.add(workListDomain);
					childlist.add(mylst);
				}
				
					
					
					workList.setAdapter(new WorkExpndListAdaptr(context, parentList,childlist, workList,lst));
					WorkListActivity.edtOrderId.setOnTouchListener(new OnTouchListener() {
						
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							WorkListActivity.edtOrderId.setText("");
							return false;
						}
					});
					
					WorkListActivity.btnOk.setOnClickListener(new OnClickListener() {

						

						@Override
						public void onClick(View v) {
							searchList.clear();
							childlist.clear();
							connectionDetector = new ConnectionDetector(context);
							if (!connectionDetector.isConnectingToInternet()) {
								CommonPopUp commonPopUp = new CommonPopUp(
										context, "Please activate internet...");
								commonPopUp.setCanceledOnTouchOutside(true);
								commonPopUp.show();
							}else{
							String id = WorkListActivity.edtOrderId.getText().toString();
							
							InputMethodManager imm = (InputMethodManager) context
									.getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(
									WorkListActivity.edtOrderId.getWindowToken(), 0);
							
							System.out.println("My Selected ="+id);

							for (int i = 0; i < parentList.size(); i++) {
								
								System.out.println("Program selected = "+parentList.get(i).getSeqIds());
								if (id.equalsIgnoreCase(parentList.get(i).getSeqIds())) {

									workListDomain = new WorkListDomain();
									workListDomain.setSeqIds(parentList.get(i).getSeqIds());
									workListDomain.setHistSeqIds(parentList.get(i).getHistSeqIds());
									workListDomain.setRef_no(parentList.get(i).getRef_no());
									workListDomain.setSource(parentList.get(i).getSource());
									workListDomain.setOrderState(parentList.get(i).getOrderState());
									workListDomain.setExecutionMode(parentList.get(i).getExecutionMode());
									workListDomain.setType(parentList.get(i).getType());
									workListDomain.setTask(parentList.get(i).getTask());
									workListDomain.setNameSpace(parentList.get(i).getNameSpace());
									workListDomain.setVersion(parentList.get(i).getVersion());
									workListDomain.setCurrentOrderState(parentList.get(i).getCurrentOrderState());
									workListDomain.setPriority(parentList.get(i).getPriority());
									workListDomain.setUser(parentList.get(i).getUser());
									workListDomain.setProcessStatus(parentList.get(i).getProcessStatus());
									workListDomain.setNumRemarks(parentList.get(i).getNumRemarks());
									searchList.add(workListDomain);
								}
								for(int j=0;j<searchList.size();j++)
								{
									ArrayList<WorkListDomain> mylst=new ArrayList<WorkListDomain>();
									workListDomain=new WorkListDomain();
									workListDomain.setSuspend("Suspend Order");
									workListDomain.setResume("Resume Order");
									workListDomain.setAbort("Abort Order");
									workListDomain.setCancel("Cancel Order");
									mylst.add(workListDomain);
									childlist.add(mylst);
								}
								workList.setAdapter(new WorkExpndListAdaptr(
										context, searchList,childlist, workList,lst));

							}
							}

						}
					});
				
				
				}
			
			
		
			  
	
		
		

	}
}
