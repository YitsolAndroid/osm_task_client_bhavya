/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;


import com.bm.task.client.notifications.NotificationActivity;
import com.bm.task.client.query.QueryActivity;
import com.bm.task.client.reporting.Chart;
import com.bm.task.client.reporting.ReportActivity;
import com.bm.task.client.reporting.StatusDescription;
import com.bm.task.client.restWS.server.LogoutServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.LoginActivity;
import com.bm.task.client.utils.NavDrawerListAdapter;
import com.bm.task.client.utils.NextActivity;
import com.bm.task.client.utils.ServerUrl;
import com.bm.task.client.worklist.WorkListActivity;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

public class FragmentForReporting extends Activity {

	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;


	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles = {"Worklist","Reporting","Query","Notifications"};

	private NavDrawerListAdapter adapter;
	private String selected="";
	private int flag = 0;
	private Encryption encryption;
	private ConnectionDetector connectionDetector;


	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.home_page);
		
		selected = getIntent().getExtras().getString("selected");
		flag =getIntent().getExtras().getInt("flag");


		// load slide menu items

		// nav drawer icons from resources

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

	
		

		// Recycle the typed array

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navMenuTitles);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		//mDrawerLayout.setDrawerListener(mDrawerToggle);
		getActionBar().setTitle("");
		getActionBar().setIcon(android.R.color.transparent);
		
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#B0C4DE")); 
		getActionBar().setBackgroundDrawable(colorDrawable);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.action_settings:
			return true;
			
		case R.id.logout:
			connectionDetector = new ConnectionDetector(FragmentForReporting.this);
			if (!connectionDetector.isConnectingToInternet()) {
				CommonPopUp commonPopUp = new CommonPopUp(
						FragmentForReporting.this, "Please activate internet...");
				commonPopUp.setCanceledOnTouchOutside(true);
				commonPopUp.show();
			}else{
			
			encryption =new Encryption();
			String encoded;
			try {
				encoded = encryption.encrypt(NextActivity.userName + ":"
						+ NextActivity.password);
				new LogoutServer(FragmentForReporting.this, encoded, CheckConnectivity.server,CheckConnectivity.port)
				.execute(ServerUrl.serverUrlXml + "logout");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			startActivity(new Intent(FragmentForReporting.this,LoginActivity.class));
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		
		System.out.println("What here "+selected +flag);
		Fragment fragment = null;
		connectionDetector = new ConnectionDetector(FragmentForReporting.this);
		if (!connectionDetector.isConnectingToInternet()) {
			CommonPopUp commonPopUp = new CommonPopUp(
					FragmentForReporting.this, "Please activate internet...");
			commonPopUp.setCanceledOnTouchOutside(true);
			commonPopUp.show();
		}else{
		if(selected.equalsIgnoreCase("orders") && flag==1 )
		{
			fragment = new StatusDescription(FragmentForReporting.this);
			flag=0;
			setTitle("Reporting");
		}
		else if(selected.equalsIgnoreCase("chart") && flag==1 )
		{
			fragment = new Chart(FragmentForReporting.this);
			flag=0;
			setTitle("Reporting");
		}
		else
		{
		switch (position) {
		case 0:
			fragment = new WorkListActivity(FragmentForReporting.this);
			break;
		case 1:
			fragment = new ReportActivity(FragmentForReporting.this);
			break;
		case 2:
			fragment = new QueryActivity(FragmentForReporting.this);
			break;
		case 3:
			fragment = new NotificationActivity(FragmentForReporting.this);
			break;
	

		default:
			break;
		}
		setTitle(navMenuTitles[position]);
		}
		
		

		if (fragment != null) {
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
		}
		
	}

	

	@Override
	public void setTitle(CharSequence title) {
		
		mTitle = title;
		getActionBar().setTitle(mTitle);
		
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	public void onBackPressed() {
		finish();
		}



	
}
