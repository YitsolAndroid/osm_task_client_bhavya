/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.restWS.server.GetReceiveOrderServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.ServerUrl;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

public class PopupOrders extends Activity {

	private TextView txtHeading;
	private TextView txtOrderId;
	private Button btnCancel;
	private Button btnOk;
	private EditText edtReason;

	private String id;
	private String orderName;
	protected String encodeData;
	protected ConnectionDetector connectionDetector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.popup_orders);
		id = getIntent().getExtras().getString("orderId");
		orderName = getIntent().getExtras().getString("order");

		txtHeading = (TextView) findViewById(R.id.txt_heading);
		txtOrderId = (TextView) findViewById(R.id.orderId);
		btnCancel = (Button) findViewById(R.id.buttonCancel);
		btnOk = (Button) findViewById(R.id.buttonOk);

		txtHeading.setText("" + orderName);
		txtOrderId.setText("Order Id : " + id);

		edtReason = (EditText) findViewById(R.id.edt_reason);

		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				connectionDetector = new ConnectionDetector(PopupOrders.this);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							PopupOrders.this, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{


				String reason = edtReason.getText().toString();

				InputMethodManager imm = (InputMethodManager) PopupOrders.this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtReason.getWindowToken(), 0);
				if (reason.equalsIgnoreCase("")) {
					Toast.makeText(getApplicationContext(),
							"Please fill the Reason", Toast.LENGTH_SHORT)
							.show();
				} else {

					if (orderName.equalsIgnoreCase("Suspend Order")) {

						Encryption encryption = new Encryption();
						try {
							String encoded = encryption
									.encrypt(FragmentDisplayActivity.userName + ":"
											+ FragmentDisplayActivity.password);

							new GetReceiveOrderServer(PopupOrders.this,
									FragmentDisplayActivity.userName,
									FragmentDisplayActivity.password, id,
									CheckConnectivity.server, CheckConnectivity.port,
									WorkExpndListAdaptr.task, encoded,
									"suspendOrder", reason)
									.execute(ServerUrl.serverUrlXml
											+ "getReceiveOrder");

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
					if (orderName.equalsIgnoreCase("Cancel Order")) {
						/**
						 * To Cancel an Order
						 */

						Encryption encryption = new Encryption();
						try {
							String encoded = encryption
									.encrypt(FragmentDisplayActivity.userName + ":"
											+ FragmentDisplayActivity.password);

							new GetReceiveOrderServer(PopupOrders.this,
									FragmentDisplayActivity.userName,
									FragmentDisplayActivity.password, id,
									CheckConnectivity.server, CheckConnectivity.port,
									WorkExpndListAdaptr.task, encoded,
									"cancelOrder", reason)
									.execute(ServerUrl.serverUrlXml
											+ "getReceiveOrder");

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
					if (orderName.equalsIgnoreCase("Resume Order")) {
						/**
						 * To resume an Order
						 */

						Encryption encryption = new Encryption();
						try {
							String encoded = encryption
									.encrypt(FragmentDisplayActivity.userName + ":"
											+ FragmentDisplayActivity.password);

							new GetReceiveOrderServer(PopupOrders.this,
									FragmentDisplayActivity.userName,
									FragmentDisplayActivity.password, id,
									CheckConnectivity.server, CheckConnectivity.port,
									WorkExpndListAdaptr.task, encoded,
									"resumeOrder", reason)
									.execute(ServerUrl.serverUrlXml
											+ "getReceiveOrder");

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
					if (orderName.equalsIgnoreCase("Abort Order")) {
						/**
						 * To abort an Order
						 */

						Encryption encryption = new Encryption();
						try {
							String encoded = encryption
									.encrypt(FragmentDisplayActivity.userName + ":"
											+ FragmentDisplayActivity.password);

							new GetReceiveOrderServer(PopupOrders.this,
									FragmentDisplayActivity.userName,
									FragmentDisplayActivity.password, id,
									CheckConnectivity.server, CheckConnectivity.port,
									WorkExpndListAdaptr.task, encoded,
									"abortOrder", reason)
									.execute(ServerUrl.serverUrlXml
											+ "getReceiveOrder");

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
				}
				}

			}
		});

	}

}
