package com.bm.task.client.restWS.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;


import com.bm.task.client.restWS.GetRemarksRWS;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.worklist.RemarkAdapter;
import com.bm.task.client.worklist.WorkExpndListAdaptr;
import com.bm.task.client.xml.RemarkDomain;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;
import src.main.java.com.bm.task.client.xml.XMLParser;

@SuppressLint("ClickableViewAccessibility")
public class GetRemarkServer extends AsyncTask<String, Void, String> {
	
	ProgressDialog progressDialog;
	String result;
	Context context;
	JSONObject json;
	private String userName;
	private String password;
	private String id;
	 public static String xml;
	private List<RemarkDomain> remarkList = new ArrayList<RemarkDomain>();
	private ListView remarkLstVw;
	private String encoded;
	
	private static final String KEY_ITEM_Error = "error";
	private static final String KEY_ITEM_Error1 = "GetOrder.Error";
	private static final String Error = "Error";

	

	public GetRemarkServer(Context context,String userName,String password,String id,ListView remarkLstVw,String encoded) {
	this.userName=userName;
	this.password = password;
	this.id=id;
	this.context=context;
	this.encoded = encoded;
	this.remarkLstVw = remarkLstVw;
	}

	protected void onPreExecute() {

		 progressDialog = new ProgressDialog(context, R.style.MyTheme);
		 progressDialog.setCancelable(false); progressDialog
		 .setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		 progressDialog.show();

	}

	@Override
	protected String doInBackground(String... urls) {
		try {

			System.out.println( ":::::::::::Coming Here :::::::::::::");
			// make web service connection
			CheckConnectivity checkConnectivity = new CheckConnectivity();
			if(checkConnectivity.Connectivity() == true){
			HttpPost request = new HttpPost(urls[0]);
			request.setHeader("Auth",encoded);
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");
			request.setHeader("Accept-Encoding", "gzip");
			// Build JSON string
			JSONStringer TestApp = new JSONStringer().object()
					.key("username").value(userName)
					.key("password").value(password)
					.key("orderId").value(id)
//					.key("remark").value("true")
					.key("task").value(WorkExpndListAdaptr.task)
					.key("accept").value("true")
					.key("user").value(WorkExpndListAdaptr.user)
					.endObject();
			StringEntity entity = new StringEntity(TestApp.toString());

			request.setEntity(entity);

		//	Log.d("****Parameter Input****", "Testing:" + TestApp);
			StringBuilder sb = new StringBuilder();
			// Send request to WCF service
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);
			if(response.getStatusLine().toString().equalsIgnoreCase("HTTP/1.1 200 OK"))
			{
				// Get the status of web service
				BufferedReader br = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				// print status in log
				String line = br.readLine();
				sb.append(line);
				result = sb.toString();
			}
			else
			{
				
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", response.getStatusLine().toString());
				result = jsonObject.toString();
			}
			}else
			{
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", "OSM is not connected");
				result = jsonObject.toString();
			}



		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public void onPostExecute(String result) {

		progressDialog.dismiss();
		       
				try {
					 JSONObject json = new JSONObject(result);
					xml = XML.toString(json);
					System.out.println(xml);  
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}if(xml.startsWith("<status>")){
					xml = "<error>"+xml+"</error>";
				}
				System.out.println("My XML is ::: ::: "+xml);  
				XMLParser xmlParser = new XMLParser();
				Document doc = xmlParser.getDomElement(xml);
		       

				NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
				NodeList error1 = doc.getElementsByTagName(KEY_ITEM_Error1);
				if (error.getLength() >= 1 || error1.getLength() >= 1) {
					if(error.getLength()>=1){
							String desc,code;
							Element e = null;
							
							for (int position = 0; position < error.getLength(); position++) {
								e = (Element) error.item(position);
							}
							
							code = xmlParser.getValue(e, "status");
							desc = xmlParser.getValue(e, "errorMsg");
						
							CommonPopUp commonPopUp = new CommonPopUp(
									context,"\nOrderID : "+id+"\nError Code : "+code +"\nDescription : "+desc	+"\n");
							commonPopUp.setCanceledOnTouchOutside(true);
							commonPopUp.show();
						
					}
					if(error1.getLength()>=1){
					String desc,code;
					Element e = null;
					NodeList nl = doc.getElementsByTagName(Error);
					
					for (int position = 0; position < nl.getLength(); position++) {
						e = (Element) nl.item(position);
					}
					
					desc = xmlParser.getValue(e, "desc");
					code = xmlParser.getValue(e, "code");
				
					CommonPopUp commonPopUp = new CommonPopUp(
							context,"\nid : "+id+"\nError Code : "+code +"\nDescription : "+desc	+"\n");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
					}
					
				}else
				{
				
				GetRemarksRWS getRws =new GetRemarksRWS(xml);
				remarkList = getRws.getRemarkRWS();
				
				remarkLstVw.setAdapter(new RemarkAdapter(context,remarkList));
				}
				
				

				
				       
				
				
		            
			
		
			  
	
		
		

	}
}
