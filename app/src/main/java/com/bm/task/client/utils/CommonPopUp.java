/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import com.example.osm_xmlapi.R;

public class CommonPopUp extends Dialog {
	private TextView tv_Msg;
	
	public CommonPopUp(Context context,String msg) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.common_popup);

		
		tv_Msg = (TextView) findViewById(R.id.tv_Msg);
		tv_Msg.setText(""+msg);
	}
	
	
}
