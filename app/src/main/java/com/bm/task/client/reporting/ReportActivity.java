/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.reporting;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.restWS.server.GetReportServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.utils.FontTypeBold;
import com.bm.task.client.utils.ServerUrl;
import com.example.osm_xmlapi.R;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
/**
 * Activity class for Status List and Chart
 * @author Bhavya
 *
 */
public class ReportActivity extends Fragment implements OnClickListener {

	private LinearLayout ll_orders;
	private LinearLayout ll_statistics;
	private ConnectionDetector connectionDetector;

	private Context context;
	private ViewGroup root;
	private Encryption encryption;

	public ReportActivity(Context context) {
		this.context = context;
	}

	@SuppressLint({ "NewApi", "CutPasteId" })
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.reporting, container, false);

		root = (ViewGroup) rootView.findViewById(R.id.ll_selectPage);
		new FontType(context, root);

		ll_orders = (LinearLayout) rootView.findViewById(R.id.ll_orders);
		root = (ViewGroup) rootView.findViewById(R.id.ll_orders);
		new FontTypeBold(context, root);
		ll_statistics = (LinearLayout) rootView
				.findViewById(R.id.ll_statistics);
		root = (ViewGroup) rootView.findViewById(R.id.ll_statistics);
		new FontTypeBold(context, root);

		ll_orders.setOnClickListener(this);
		ll_statistics.setOnClickListener(this);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy); // To allow data access

		connectionDetector = new ConnectionDetector(context);

		return rootView;

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.ll_orders:
			gotoXMLFile("orders");
			break;

		case R.id.ll_statistics:
			gotoXMLFile("chart");
			break;

		default:
			break;
		}

	}

	private void gotoXMLFile(String orderState) {

		if (!connectionDetector.isConnectingToInternet()) {
			CommonPopUp commonPopUp = new CommonPopUp(context,
					"Please activate internet...");
			commonPopUp.setCanceledOnTouchOutside(true);
			commonPopUp.show();
		} else {

			encryption = new Encryption();
			try {
				String encoded = encryption
						.encrypt(FragmentDisplayActivity.userName + ":"
								+ FragmentDisplayActivity.password);

				new GetReportServer(context, encoded, orderState)
						.execute(ServerUrl.serverUrl + "reportForOrderState");

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

	}

	public void onBackPressed() {

	}

}
