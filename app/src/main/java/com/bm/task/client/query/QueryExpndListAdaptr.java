/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.query;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;


import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.fragment.FragmentForWorkList;
import com.bm.task.client.restWS.server.GetOrderQueryServerXML;
import com.bm.task.client.restWS.server.GetQueryProcessHistoryServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.ServerUrl;
import com.bm.task.client.worklist.PopupOrders;
import com.bm.task.client.xml.WorkListDomain;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

@SuppressLint("DefaultLocale")
public class QueryExpndListAdaptr extends BaseExpandableListAdapter implements
		OnClickListener {

	private Context con;
	
	private ExpandableListView workList;
	
	private ArrayList<ArrayList<WorkListDomain>> childList;
	public static List<WorkListDomain> taskList = new ArrayList<WorkListDomain>();
	private List<WorkListDomain> lst;

	public static String user;
	public static String orderState;
	public static String task;
	public static String orderSource;
	public static String priority;
	public static String ref;
	public static String process;
	public static String type;
	public static String id;
	
	private String childOrderId;

	protected ConnectionDetector connectionDetector;

	public QueryExpndListAdaptr(Context con, List<WorkListDomain> lst,
			ArrayList<ArrayList<WorkListDomain>> childList,
			ExpandableListView workList) {

		this.con = con;
		this.lst = lst;
		this.childList = childList;
		this.workList = workList;

	}

	@Override
	public int getGroupCount() {
		return lst.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return childList.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return lst.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return childList.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getGroupView(final int position, final boolean isExpanded,
			View v, final ViewGroup parent) {
		if (v == null) {
			LayoutInflater in = (LayoutInflater) con
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = in.inflate(R.layout.txt_querylist, null);

		}
		TextView txt_id = (TextView) v.findViewById(R.id.txt_id);
		TextView txt_orderState = (TextView) v
				.findViewById(R.id.txt_orderState);
		TextView txt_user = (TextView) v.findViewById(R.id.txt_user);
		TextView txt_type = (TextView) v.findViewById(R.id.txt_Type);
		TextView txt_CompletedDate = (TextView) v.findViewById(R.id.txt_CompletedDate);
		TextView txt_state = (TextView) v.findViewById(R.id.txt_state);
		TextView txt_Task = (TextView)v.findViewById(R.id.txt_orderTask);
		ImageView img_indicator = (ImageView) v
				.findViewById(R.id.img_indicator);
		ImageView img_attach = (ImageView) v
				.findViewById(R.id.img_attach);
		
		if(lst.get(position).getNumRemarks().equalsIgnoreCase("0"))
		{
			img_attach.setVisibility(View.GONE);
		}
		else
		{
			img_attach.setVisibility(View.VISIBLE);
		}
		
		img_attach.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
			}
		});

		txt_id.setText("Id : " + lst.get(position).getSeqIds());
		txt_user.setText("User : " + lst.get(position).getUser());
		txt_CompletedDate.setText("Completed : " + lst.get(position).getExpectedOrderCompletionDate());
		txt_type.setText("Type : " + lst.get(position).getType());
		txt_Task.setText("Task : " + lst.get(position).getTask());

		String currentOrderState = lst.get(position).getCurrentOrderState();
		String status = lst.get(position).getOrderState();
		status = status.substring(0, 1).toUpperCase() + status.substring(1);
		String[] state = currentOrderState.split("\\.");
		String currentState = state[state.length - 1];
		currentState = currentState.substring(0, 1).toUpperCase()
				+ currentState.substring(1);
		currentState = currentState.replaceAll("_", "");

		txt_state.setText("OrderState : " + currentState);
		txt_orderState.setText("State : " + status);

		if (currentState.equalsIgnoreCase("Suspended")
				|| currentState.equalsIgnoreCase("Cancelled")
				|| currentState.equalsIgnoreCase("Cancelling")) {
			txt_state.setTextColor(Color.RED);
		} 

		

		if (position % 2 == 1) {

			v.setBackgroundColor(Color.parseColor("#ffffff"));
		} else {
			v.setBackgroundColor(Color.parseColor("#e2e2e2"));
		}

		img_indicator.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isExpanded)
					((ExpandableListView) parent).collapseGroup(position);
				else
					((ExpandableListView) parent).expandGroup(position, true);

			}
		});

		/**
		 * Click on Parent List
		 */
		workList.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int position, long ids) {

				user = lst.get(position).getUser();
				task = lst.get(position).getTask();
				orderSource = lst.get(position).getSource();
				priority = lst.get(position).getPriority();
				ref = lst.get(position).getRef_no();
				process = lst.get(position).getProcessStatus();
				String currentOrderState = lst.get(position)
						.getCurrentOrderState();
				String[] state = currentOrderState.split("\\.");
				orderState = state[state.length - 1];
				type=lst.get(position).getType();
				id  = lst.get(position).getSeqIds();

				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
						.permitAll().build();
				StrictMode.setThreadPolicy(policy);
				
				taskList.clear();
				
				connectionDetector = new ConnectionDetector(con);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							con, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{

				if (QueryLstActivity.selected.equalsIgnoreCase("Editor")) {
					
					
					Encryption encryption = new Encryption();
					try {
						String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetOrderQueryServerXML(con, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
							lst.get(position).getSeqIds(), CheckConnectivity.server,CheckConnectivity.port,"true",lst.get(position).getTask(),encoded,lst.get(position).getUser()).
							execute(ServerUrl.serverUrlXml + "getOrder");
						
						
						
					
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					
					
				}
				if (QueryLstActivity.selected.equalsIgnoreCase("Add Remark")) {
					Intent in = new Intent(con, FragmentForWorkList.class);
					in.putExtra("selected", "addremark");
					in.putExtra("flag", 1);
					con.startActivity(in);
				}

				if (QueryLstActivity.selected
						.equalsIgnoreCase("Process History")) {
					Encryption encryption = new Encryption();
					try {
						String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetQueryProcessHistoryServer(con, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
							lst.get(position).getSeqIds(),CheckConnectivity.server,CheckConnectivity.port, lst.get(position).getRef_no(),encoded).execute(ServerUrl.serverUrlXml + "getOrderProcessHistory");
					
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				}

				return true;

			}

		});

		return v;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getChildView(final int position, int childPosition,
			boolean isLastChild, View v, ViewGroup parent) {
		if (v == null) {
			LayoutInflater in = (LayoutInflater) con
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = in.inflate(R.layout.txt_child, null);
		}
		childOrderId = lst.get(position).getSeqIds();
		user = lst.get(position).getUser();
		task = lst.get(position).getTask();
		orderSource = lst.get(position).getSource();
		priority = lst.get(position).getPriority();
		ref = lst.get(position).getRef_no();
		process = lst.get(position).getProcessStatus();
		String currentOrderState = lst.get(position)
				.getCurrentOrderState();
		String[] state = currentOrderState.split("\\.");
		orderState = state[state.length - 1];
		type=lst.get(position).getType();
		TextView tvSuspend = (TextView) v.findViewById(R.id.txt_suspend);
		TextView tvCancel = (TextView) v.findViewById(R.id.txt_cancel);
		TextView tvResume = (TextView) v.findViewById(R.id.txt_resume);
		TextView tvAbort = (TextView) v.findViewById(R.id.txt_abort);
		tvSuspend.setText(""
				+ childList.get(position).get(childPosition).getSuspend());
		tvCancel.setText(""
				+ childList.get(position).get(childPosition).getCancel());
		tvResume.setText(""
				+ childList.get(position).get(childPosition).getResume());
		tvAbort.setText(""
				+ childList.get(position).get(childPosition).getAbort());

		tvSuspend.setOnClickListener(QueryExpndListAdaptr.this);
		tvCancel.setOnClickListener(QueryExpndListAdaptr.this);
		tvResume.setOnClickListener(QueryExpndListAdaptr.this);
		tvAbort.setOnClickListener(QueryExpndListAdaptr.this);

		return v;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.txt_suspend:
			gotoChangeOrders("Suspend Order");
			break;

		case R.id.txt_cancel:
			gotoChangeOrders("Cancel Order");

			break;

		case R.id.txt_resume:
			gotoChangeOrders("Resume Order");

			break;
			
		case R.id.txt_abort:
			gotoChangeOrders("Abort Order");

			break;

		default:
			break;
		}

	}
	
	/**
	 * 
	 * for Changing OrderState 
	 * @param order
	 */

	private void gotoChangeOrders(String order) {

		Intent in = new Intent(con, PopupOrders.class);
		in.putExtra("orderId", childOrderId);
		in.putExtra("order", order);
		con.startActivity(in);

	}

}
