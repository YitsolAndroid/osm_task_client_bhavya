package com.bm.task.client.worklist;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.fragment.FragmentForWorkList;
import com.bm.task.client.restWS.server.GetOrderServerXML;
import com.bm.task.client.restWS.server.GetRemarkServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.utils.ServerUrl;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

@SuppressLint("ValidFragment")
public class RemarkActivity extends Fragment{
	
	private String id;
	private String ref;
	private TextView txtId;
	private TextView txtRef;
	private ListView remarkLst;
	private Button editOrder;
	private Button modifyOrder;
	private Button addRemark;
	private Context context;
	private ViewGroup root;
	protected ConnectionDetector connectionDetector;
	
	 public RemarkActivity(Context context,String id,String ref){
		 this.context = context;
		 this.id =id;
		 this.ref =ref;
	 }


	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.display_remark, container, false);
	        
	        
	        root = (ViewGroup)rootView.findViewById(R.id.ll_displayremark);
	        new FontType(context, root);
		
		editOrder =(Button)rootView.findViewById(R.id.btn_editOrder);
		modifyOrder =(Button)rootView.findViewById(R.id.btn_modify_remark);
		addRemark =(Button)rootView.findViewById(R.id.btn_add_remark);
		
		txtId= (TextView)rootView.findViewById(R.id.txt_remark_id);
		txtRef = (TextView)rootView.findViewById(R.id.txt_remark_ref);
		remarkLst = (ListView)rootView.findViewById(R.id.lst_remark);
		
		txtId.setText("Order ID : "+id);
		txtRef.setText("Ref #. : "+ref);
		
		addRemark.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				connectionDetector = new ConnectionDetector(context);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							context, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{
				Intent in = new Intent(context, FragmentForWorkList.class);
				in.putExtra("selected", "addremark");
				in.putExtra("flag", 1);
				startActivity(in);
				}
				
				
			}
		});
		
		editOrder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				connectionDetector = new ConnectionDetector(context);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							context, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{
				
				Encryption encryption = new Encryption();
				try {
					String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetOrderServerXML(context, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
							WorkExpndListAdaptr.id, CheckConnectivity.server,CheckConnectivity.port,"true",WorkExpndListAdaptr.task,encoded,WorkExpndListAdaptr.user).
							execute(ServerUrl.serverUrlXml + "getOrder");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
				
			}
		});
		
		modifyOrder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
		
		Encryption encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
		
		new GetRemarkServer(context, FragmentDisplayActivity.userName,FragmentDisplayActivity.password, 
				id,remarkLst,encoded).execute(ServerUrl.serverUrlXml
		+ "getOrder");
		
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		return rootView;

		
		
	}

}
