/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.fragment.FragmentForWorkList;
import com.bm.task.client.restWS.server.GetAcceptOrderServer;
import com.bm.task.client.restWS.server.GetAssignOrderServer;
import com.bm.task.client.restWS.server.GetCompleteOrderServer;
import com.bm.task.client.restWS.server.GetOrderServerXML;
import com.bm.task.client.restWS.server.GetReceiveOrderServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.utils.ServerUrl;

import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;
import src.main.java.com.bm.task.client.xml.GetListStatus;

@SuppressLint("ValidFragment")
public class ChangeStatus extends Fragment {

	private Button btnUpdate;
	private Button btnEditOrder;


	private TextView txtTask;
	private TextView txtRef;
	private TextView txtId;

	private String id;
	private String xml;
	private List<String> headList;
	private Spinner spnrTask;
	private ListView lstvStatus;
	protected String task ;
	private Context context;
	protected ConnectionDetector connectionDetector;

	public ChangeStatus(Context context,String id,String xml)
	{
		this.id=id;
		this.xml=xml;
		this.context = context;
	}
	@Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.change_status, container, false);

		txtId = (TextView) rootView.findViewById(R.id.txt_OrderId);
		txtRef = (TextView) rootView.findViewById(R.id.txt_Ref);
		txtTask = (TextView) rootView.findViewById(R.id.txt_Task);
		spnrTask = (Spinner) rootView.findViewById(R.id.spn_task);
		txtId.setText("Order ID : " + id);
		txtRef.setText("Ref #. : " + WorkExpndListAdaptr.ref);
		txtTask.setText("Task : " + WorkExpndListAdaptr.task);
		
		ViewGroup root = (ViewGroup) rootView.findViewById(R.id.ll_changeStatus);
		new FontType(context, root);

		btnUpdate = (Button) rootView.findViewById(R.id.btn_update);
		btnEditOrder = (Button) rootView.findViewById(R.id.btn_EditOrder);


		lstvStatus = (ListView) rootView.findViewById(R.id.list_status);

		GetListStatus getListStatus = new GetListStatus();
		headList = getListStatus.getStatus(xml);
		
		spnrTask.setAdapter(new SpnAssignAdapter(context, headList));
		
		spnrTask.setOnItemSelectedListener(new OnItemSelectedListener() {

			


			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				
				ChangeStatusAdapter.checked = "No Selection";
				
				task = parent.getAdapter().getItem(position).toString();
				
				lstvStatus.setAdapter(new ChangeStatusAdapter(context, GetListStatus.lst.get(position),task));
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		

	

		btnEditOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				connectionDetector = new ConnectionDetector(context);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							context, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{
				
				Encryption encryption = new Encryption();
				try {
					String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
				
				new GetOrderServerXML(context, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
						WorkExpndListAdaptr.id, CheckConnectivity.server,CheckConnectivity.port,"true",WorkExpndListAdaptr.task,encoded,WorkExpndListAdaptr.user).
						execute(ServerUrl.serverUrlXml + "getOrder");
				/*new GetOrderServer(context,  FragmentDisplayActivity.userName,FragmentDisplayActivity.password, WorkExpndListAdaptr.id, encoded)
				.execute(ServerUrl.serverUrl+"getOrder");*/
				
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				Intent in = new Intent(context,
						FragmentForWorkList.class);
				in.putExtra("selected", "editor");
				in.putExtra("flag", 1);
				context.startActivity(in);
				}

			}
		});
		
		
		btnUpdate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				String checked = ChangeStatusAdapter.checked;
				connectionDetector = new ConnectionDetector(context);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							context, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{
				if (checked.equalsIgnoreCase("No Selection")) {
					Toast.makeText(context, "Please Select", Toast.LENGTH_SHORT).show();

				}
				
				else
				{
				if (task.equalsIgnoreCase("Assigned")) {
					
					System.out.println("*********Assigned "+checked);
					Encryption encryption = new Encryption();
					try {
						String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetAssignOrderServer(context, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
							id,CheckConnectivity.server,CheckConnectivity.port,checked,WorkExpndListAdaptr.task,encoded).
							execute(ServerUrl.serverUrlXml + "getAssignOrder");
					
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
				if (task.equalsIgnoreCase("Received")) {
					System.out.println("*********Received ");
					Encryption encryption = new Encryption();
					try {
						String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetReceiveOrderServer(context, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
							id,CheckConnectivity.server,CheckConnectivity.port,WorkExpndListAdaptr.task,encoded,"","").
							execute(ServerUrl.serverUrlXml + "getReceiveOrder");
					
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
				if (task.equalsIgnoreCase("Completed")) {
					
					
					System.out.println("*********Completed "+checked);
					
					Encryption encryption = new Encryption();
					try {
						String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetCompleteOrderServer(context, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
							id,CheckConnectivity.server,CheckConnectivity.port,checked,WorkExpndListAdaptr.task,encoded,"worklist").
							execute(ServerUrl.serverUrlXml + "getCompleteOrder");
					
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
				if (task.equalsIgnoreCase("Accepted")) {
					Encryption encryption = new Encryption();
					try {
						String encoded = encryption.encrypt(FragmentDisplayActivity.userName+":"+FragmentDisplayActivity.password);
					
					new GetAcceptOrderServer(context, FragmentDisplayActivity.userName, FragmentDisplayActivity.password, 
							id,CheckConnectivity.server,CheckConnectivity.port,"true",WorkExpndListAdaptr.task,encoded,WorkExpndListAdaptr.user).
							execute(ServerUrl.serverUrlXml + "getOrder");
					
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
				}
				}
				
			}
		});

		
		return rootView;

	}
	
	public void onBackPressed() {
		
		Intent intent = new Intent(context,
				FragmentDisplayActivity.class);
		intent.putExtra("selected", "worklist");
		intent.putExtra("flag", 1);
		startActivity(intent);

		

	}
}
