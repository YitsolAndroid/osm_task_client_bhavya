/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 
package com.bm.task.client.worklist;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.bm.task.client.xml.GetListStatus;
import com.example.osm_xmlapi.R;

public class ChangeStatusAdapter1 extends BaseAdapter {

	private Context con;
	private Integer selected=-1;
	public static String checked="No Selection";
	private int pos;
	protected String str="";
	protected Integer select;
	private ArrayAdapter<String> adapter;
	public static List<String> lst;
	public static List<String> spnSelectLst=new ArrayList<String>();

	@SuppressWarnings("static-access")
	public ChangeStatusAdapter1(Context con, List<String> headList) {
		this.con = con;
		this.lst = headList;
	}
	
	@Override
	public int getCount() {
		return lst.size();

	}

	@Override
	public Object getItem(int position) {
		return lst.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		if (v == null) {
			LayoutInflater in = (LayoutInflater) con
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = in.inflate(R.layout.txt_change_status, null);
		}
		
		RadioButton rb = (RadioButton) v.findViewById(R.id.rb_row);
		rb.setText(""+lst.get(position));
		Spinner spnr = (Spinner) v.findViewById(R.id.spinner_row);
		
		if(GetListStatus.lst.get(position).get(0).equalsIgnoreCase(""))
		{
			spnr.setVisibility(View.GONE);
		}
		else
		{
			adapter=new ArrayAdapter<String>(con,android.R.layout.simple_spinner_item,GetListStatus.lst.get(position));
			//adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            spnr.setAdapter(adapter);
            
            
		}
		
		
		spnr.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				
				String str = parent.getItemAtPosition( position).toString();
				
				 System.out.println( "Item: " + parent.getItemAtPosition( position).toString());
				 
				 spnSelectLst.add(str);
				 System.out.println("comng"+spnSelectLst.size());
			
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		

		
		rb.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				selected = (Integer) v.getTag();
	            notifyDataSetChanged();				
			}
		});
		rb.setTag(position);
		rb.setChecked(position == selected);

		if(rb.isChecked())
		{
			checked = rb.getText().toString();
			System.out.println("Radio Button Checked is *******"+checked);
		}
		
		return v;
	}
	
	
}
*/