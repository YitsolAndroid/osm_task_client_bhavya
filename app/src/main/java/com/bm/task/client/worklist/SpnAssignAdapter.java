/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.osm_xmlapi.R;

public class SpnAssignAdapter extends BaseAdapter {
	
	private Context con;
	public static List<String> lst;

	@SuppressWarnings("static-access")
	public SpnAssignAdapter(Context con,	List<String> lst){
		
		this.con=con;
		this.lst=lst;
		
	}

	@Override
	public int getCount() {
		return lst.size();
		
	}

	@Override
	public Object getItem(int position) {
		return lst.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		if(v==null)
		{
			LayoutInflater in=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v=in.inflate(R.layout.txt_spn, null);
			
		}
		TextView txt=(TextView)v.findViewById(R.id.txt);
		txt.setText(""+lst.get(position));
		
	return v;
	}
	}
