package com.bm.task.client.restWS.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;


import com.bm.task.client.fragment.FragmentForReporting;
import com.bm.task.client.reporting.StatusDomain;
import com.bm.task.client.utils.CommonPopUp;

import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;
import src.main.java.com.bm.task.client.xml.XMLParser;

@SuppressLint("ClickableViewAccessibility")
public class GetReportServer extends AsyncTask<String, Void, String> {

	ProgressDialog progressDialog;
	String result;
	Context context;
	JSONObject json;
	public static String xml;
	private String encoded;
	private List<StatusDomain> orderStatelist = new ArrayList<StatusDomain>();

	private String selectedState;
	
	public static List<StatusDomain> totalList = new ArrayList<StatusDomain>();
	public static List<StatusDomain> completedList = new ArrayList<StatusDomain>();
	public static List<StatusDomain> pendingList = new ArrayList<StatusDomain>();
	public static List<StatusDomain> failedList = new ArrayList<StatusDomain>();
	public static int total;
	public static int completed;
	public static int failed;
	public static int pending;
	StatusDomain statusDomain;
	
	private static final String KEY_ITEM_Error = "error";
	private static final String KEY_ITEM_Error1 = "reportForOrderState.Error";
	private static final String Error = "Error";


	public GetReportServer(Context context, String encoded, String selectedState) {

		this.encoded = encoded;
		this.context = context;
		this.selectedState = selectedState;

	}

	protected void onPreExecute() {

		progressDialog = new ProgressDialog(context, R.style.MyTheme);
		progressDialog.setCancelable(false);
		progressDialog
				.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		progressDialog.show();

	}

	@Override
	protected String doInBackground(String... urls) {
		try {

			System.out.println(":::::::::::Coming Here :::::::::::::");
			System.out.println(":::::::::::" + encoded + " :::::::::::::");
			CheckConnectivity checkConnectivity = new CheckConnectivity();
			if(checkConnectivity.Connectivity() == true){
			// make web service connection
			HttpGet request = new HttpGet(urls[0]);
			request.setHeader("Auth", encoded);
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");
			request.setHeader("Accept-Encoding", "gzip");
			// Build JSON string

			StringBuilder sb = new StringBuilder();
			// Send request to WCF service
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);

			if(response.getStatusLine().toString().equalsIgnoreCase("HTTP/1.1 200 OK"))
			{
				// Get the status of web service
				BufferedReader br = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				// print status in log
				String line = br.readLine();
				sb.append(line);
				result = sb.toString();
			}
			else
			{
				
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", response.getStatusLine().toString());
				result = jsonObject.toString();
			}
			}else
			{
				JSONObject jsonObject =new JSONObject();
				jsonObject.put("status", "OSM is not connected");
				result = jsonObject.toString();
			}



		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public void onPostExecute(String result) {
		progressDialog.dismiss();

		if (result.startsWith("{\"status")) {
		try {
			JSONObject json =new JSONObject(result);
			xml = XML.toString(json);
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
			xml = "<error>" + xml + "</error>";
		
		System.out.println("My XML is ::: ::: " + xml);
		XMLParser xmlParser = new XMLParser();
		Document doc = xmlParser.getDomElement(xml);

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		NodeList error1 = doc.getElementsByTagName(KEY_ITEM_Error1);
		if (error.getLength() >= 1 || error1.getLength() >= 1) {
			if (error.getLength() >= 1) {
				String desc, code;
				Element e = null;

				for (int position = 0; position < error.getLength(); position++) {
					e = (Element) error.item(position);
				}

				code = xmlParser.getValue(e, "status");
				desc = xmlParser.getValue(e, "errorMsg");

				CommonPopUp commonPopUp = new CommonPopUp(context,
						"\nError Code : " + code
								+ "\nDescription : " + desc + "\n");
				commonPopUp.setCanceledOnTouchOutside(true);
				commonPopUp.show();

			}
			if (error1.getLength() >= 1) {
				String desc, code;
				Element e = null;
				NodeList nl = doc.getElementsByTagName(Error);

				for (int position = 0; position < nl.getLength(); position++) {
					e = (Element) nl.item(position);
				}

				desc = xmlParser.getValue(e, "desc");
				code = xmlParser.getValue(e, "code");

				CommonPopUp commonPopUp = new CommonPopUp(context, "\nError Code : " + code + "\nDescription : "
						+ desc + "\n");
				commonPopUp.setCanceledOnTouchOutside(true);
				commonPopUp.show();
			}

		}
		}else{		

		System.out.println("My result is : " + result);
		JSONArray jsonArray;

		try {
			jsonArray = new JSONArray(result);
			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject json = jsonArray.getJSONObject(i);
				
				statusDomain = new StatusDomain();
				statusDomain.setStatusCount(json.getInt("total"));
				statusDomain.setStatusName(json.getString("mnemonic"));
				orderStatelist.add(statusDomain);
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pending = 0;completed=0;failed=0;total=0;
		totalList.clear();
		completedList.clear();
		pendingList.clear();
		failedList.clear();
		
		for(int i=0;i<orderStatelist.size();i++){
			
			statusDomain =new StatusDomain();
			statusDomain.setStatusName(orderStatelist.get(i).getStatusName());
			statusDomain.setStatusCount(orderStatelist.get(i).getStatusCount());
			totalList.add(statusDomain);
			
			total = total + orderStatelist.get(i).getStatusCount();
			if(orderStatelist.get(i).getStatusName().equalsIgnoreCase("not_started")||
					orderStatelist.get(i).getStatusName().equalsIgnoreCase("suspended")||
					orderStatelist.get(i).getStatusName().equalsIgnoreCase("in_progress")||
					orderStatelist.get(i).getStatusName().equalsIgnoreCase("cancelling")||
					orderStatelist.get(i).getStatusName().equalsIgnoreCase("amending")||
					orderStatelist.get(i).getStatusName().equalsIgnoreCase("cancelling")||
					orderStatelist.get(i).getStatusName().equalsIgnoreCase("waiting_for_revision")){
				
				pending = pending+orderStatelist.get(i).getStatusCount();
				statusDomain =new StatusDomain();
				statusDomain.setStatusName(orderStatelist.get(i).getStatusName());
				statusDomain.setStatusCount(orderStatelist.get(i).getStatusCount());
				pendingList.add(statusDomain);
				
			}
			if(orderStatelist.get(i).getStatusName().equalsIgnoreCase("cancelled")||
					orderStatelist.get(i).getStatusName().equalsIgnoreCase("completed")||
					orderStatelist.get(i).getStatusName().equalsIgnoreCase("aborted")){
				completed = completed +orderStatelist.get(i).getStatusCount();
				
				statusDomain =new StatusDomain();
				statusDomain.setStatusName(orderStatelist.get(i).getStatusName());
				statusDomain.setStatusCount(orderStatelist.get(i).getStatusCount());
				completedList.add(statusDomain);
			}
			if(orderStatelist.get(i).getStatusName().equalsIgnoreCase("failed")){
				failed = failed + orderStatelist.get(i).getStatusCount(); 
				statusDomain =new StatusDomain();
				statusDomain.setStatusName(orderStatelist.get(i).getStatusName());
				statusDomain.setStatusCount(orderStatelist.get(i).getStatusCount());
				failedList.add(statusDomain);
			}
		}

		if (selectedState.equalsIgnoreCase("orders")) {
			Intent intent = new Intent(context, FragmentForReporting.class);
			intent.putExtra("selected", "orders");
			intent.putExtra("flag", 1);
			context.startActivity(intent);
			System.out.println("coming heree  RE");
		}
		if (selectedState.equalsIgnoreCase("chart")) {
			Intent intent = new Intent(context, FragmentForReporting.class);
			intent.putExtra("selected", "chart");
			intent.putExtra("flag", 1);
			context.startActivity(intent);
		}
		}

	}
}
