/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.query;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.restWS.server.GetFindOrder;
import com.bm.task.client.restWS.server.GetQueryDataServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.utils.ServerUrl;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

/***
 * 
 * @author Bhavya *
 */
public class QueryActivity extends Fragment implements OnItemSelectedListener{
	
	private Spinner spnrOrderState;
	private Spinner spnrState;
	private Spinner spnrNameSpace;
	private Spinner spnrType;
	private Spinner spnrSource;
	private Spinner spnrProcessStatus;
	private Spinner spnrExecutionMode;
	private Spinner spnrUser;
	private Spinner spnrTask;
	private Encryption encryption;
	private Button btnSearch;

	private String orderState="";
	private String nameSpace="";
	private String processStatus="";
	private String source="";
	private String state="";
	private String executionMode="";
	private String task="";
	private String type="";
	private String user="";
	
	private String orderId="";
	private String createDateFrom="";
	private String CreateDateTo="";
	
	private List<String> executionList = new ArrayList<String>();
	private ArrayAdapter<String> adapter;
	protected String ref="";
	private Context context;
	private ViewGroup root;
	private EditText edtId;
	private EditText edtRef;
	protected ConnectionDetector connectionDetector;

	public QueryActivity(Context context){
		this.context = context;
	}
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.query, container, false);

		spnrOrderState = (Spinner)rootView.findViewById(R.id.spnOrderState);
		spnrState = (Spinner)rootView.findViewById(R.id.spnState);
		spnrNameSpace = (Spinner)rootView.findViewById(R.id.spnNamespace);
		spnrType = (Spinner)rootView.findViewById(R.id.spnType);
		spnrSource = (Spinner)rootView.findViewById(R.id.spnSource);
		spnrProcessStatus= (Spinner)rootView.findViewById(R.id.spnProcessStatus);
		spnrExecutionMode = (Spinner)rootView.findViewById(R.id.spnExecutionMode);
		spnrUser = (Spinner)rootView.findViewById(R.id.spnUser);
		spnrTask = (Spinner)rootView.findViewById(R.id.spnTask);
		
		edtId = (EditText)rootView.findViewById(R.id.edtId);
		edtRef = (EditText)rootView.findViewById(R.id.edtref);
		
		btnSearch = (Button) rootView.findViewById(R.id.btnSearch);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		root = (ViewGroup)rootView.findViewById(R.id.ll_query);
		new FontType(context, root);
		
		root = (ViewGroup)rootView.findViewById(R.id.ll_heading);
		new FontType(context, root);
		
		executionList.add("All");
		executionList.add("Do (In Amending)");
		executionList.add("Do");
		executionList.add("Redo");
		executionList.add("Undo");
		
		adapter=new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,executionList);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spnrExecutionMode.setAdapter(adapter);

		
	

		encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
					+ FragmentDisplayActivity.password);

			new GetQueryDataServer(context, encoded, "orderstate",spnrOrderState)
					.execute(ServerUrl.serverUrl + "getOrderState");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/*encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(NextActivity.userName + ":"
					+ NextActivity.password);

			new GetQueryDataServer(QueryActivity1.this, encoded,
					"processIdDesc",spnrProcess).execute(ServerUrl.serverUrl
					+ "getAllProcess");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/

		encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
					+ FragmentDisplayActivity.password);

			new GetQueryDataServer(context, encoded, "statedesc",spnrState)
					.execute(ServerUrl.serverUrl + "getAllOrderStates");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
					+ FragmentDisplayActivity.password);

			new GetQueryDataServer(context, encoded,
					"processIdDesc",spnrProcessStatus).execute(ServerUrl.serverUrl
					+ "getProcessStatus");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
					+ FragmentDisplayActivity.password);

			new GetQueryDataServer(context, encoded, "namespace",spnrNameSpace)
					.execute(ServerUrl.serverUrl + "getCatridges");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
					+ FragmentDisplayActivity.password);

			new GetQueryDataServer(context, encoded, "orderType",spnrType)
					.execute(ServerUrl.serverUrl + "getType");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
					+ FragmentDisplayActivity.password);

			new GetQueryDataServer(context, encoded, "source",spnrSource)
					.execute(ServerUrl.serverUrl + "getSource");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
					+ FragmentDisplayActivity.password);

			new GetQueryDataServer(context, encoded, "task",spnrTask)
					.execute(ServerUrl.serverUrl + "getTask");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
					+ FragmentDisplayActivity.password);

			new GetQueryDataServer(context, encoded, "username",spnrUser)
					.execute(ServerUrl.serverUrl + "getUsers");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/*encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(NextActivity.userName + ":"
					+ NextActivity.password);

			new GetQueryDataServer(QueryActivity1.this, encoded, "workgroup",spnrWorkgroups)
					.execute(ServerUrl.serverUrl + "getWorkgroups");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		spnrExecutionMode.setOnItemSelectedListener(this);
		spnrOrderState.setOnItemSelectedListener(this);
		spnrSource.setOnItemSelectedListener(this);
		spnrState.setOnItemSelectedListener(this);  //small letter all
		spnrProcessStatus.setOnItemSelectedListener(this);//small letter all
		spnrUser.setOnItemSelectedListener(this);
		spnrNameSpace.setOnItemSelectedListener(this);
		spnrTask.setOnItemSelectedListener(this);
		spnrType.setOnItemSelectedListener(this);
		
	
		

	

		btnSearch.setOnClickListener(new OnClickListener() {

			

			@SuppressLint("DefaultLocale")
			@Override
			public void onClick(View v) {
				
				orderId = edtId.getText().toString();
				ref = edtRef.getText().toString();
				
				
				
				if(orderState.equalsIgnoreCase("All"))
					orderState="";
				
				else if(orderState.equalsIgnoreCase("completed")||orderState.equalsIgnoreCase("aborted"))
						orderState ="closed."+orderState;
				
				else if(orderState.equalsIgnoreCase("in_progress"))
					orderState = "open.running."+orderState;
				
				else if(orderState.equalsIgnoreCase("suspended")||orderState.equalsIgnoreCase("cancelled")||
						orderState.equalsIgnoreCase("failed")||orderState.equalsIgnoreCase("not_started")||
						orderState.equalsIgnoreCase("waiting_for_revision"))
					orderState = "open.not_running."+orderState;
				
				else if(orderState.equalsIgnoreCase("cancelling")||orderState.equalsIgnoreCase("amending"))
					orderState = "open.running.compensating."+orderState;
				
				if(nameSpace.equalsIgnoreCase("All"))nameSpace="";
				if(processStatus.equalsIgnoreCase("All"))
					processStatus="";
				else
					processStatus = processStatus.toLowerCase();
				if(state.equalsIgnoreCase("All"))
					state="";
				else
					state = state.toLowerCase();
				if(source.equalsIgnoreCase("All"))source="";
				if(user.equalsIgnoreCase("All"))user="";
				if(task.equalsIgnoreCase("All"))task="";
				if(type.equalsIgnoreCase("All"))type="";
				if(executionMode.equalsIgnoreCase("All"))
					executionMode="";
				else
					executionMode = executionMode.toLowerCase();
				
				System.out.println(nameSpace +"***"+orderState +"***" +processStatus
						+"***" +state +"***"+source +"***" +user
						+"***" +task +"***"+type +"***" +executionMode);
				connectionDetector = new ConnectionDetector(context);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							context, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{


				
				
				encryption = new Encryption();
				try {
					String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
							+ FragmentDisplayActivity.password);

				new GetFindOrder(context, FragmentDisplayActivity.userName,
						FragmentDisplayActivity.password,orderId,ref, CheckConnectivity.server,CheckConnectivity.port,
						user,task,source,type,orderState,executionMode,
						nameSpace,state,processStatus,
						createDateFrom,CreateDateTo,encoded)
						.execute(ServerUrl.serverUrlXml+"queryString");
				
				System.out.println(FragmentDisplayActivity.userName + FragmentDisplayActivity.password + encoded);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}

			}
		});
		
		return rootView;

	}

	

	/*@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.img_logout:
			logoutSession(NextActivity.userName, NextActivity.password);

			break;

		case R.id.img_back:
			Intent intent = new Intent(QueryActivity1.this, NextActivity.class);
			startActivity(intent);

			break;


	}*/
	

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		
		switch(parent.getId()){
		
		case R.id.spnOrderState :
			orderState = parent.getAdapter().getItem(position).toString();
			break;
			
		case R.id.spnNamespace :
			nameSpace =  parent.getAdapter().getItem(position).toString();
			
			break;
			
		case R.id.spnProcessStatus :
			processStatus = parent.getAdapter().getItem(position).toString();
			
			break;
	
			
		case R.id.spnSource :
			source = parent.getAdapter().getItem(position).toString();
			break;
			
		case R.id.spnState :
			state = parent.getAdapter().getItem(position).toString();
			break;
			
		case R.id.spnExecutionMode :
			executionMode = parent.getAdapter().getItem(position).toString();
			break;
			
		case R.id.spnTask :
			task = parent.getAdapter().getItem(position).toString();
			break;
			
		case R.id.spnType :
			type = parent.getAdapter().getItem(position).toString();
			break;
			
		case R.id.spnUser :
			user = parent.getAdapter().getItem(position).toString();
			break;
		
		}
			
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
}
