/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import java.text.ParseException;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bm.task.client.xml.FormatDateTime;
import com.bm.task.client.xml.RemarkDomain;
import com.example.osm_xmlapi.R;

public class RemarkAdapter extends BaseAdapter {
	
	private Context con;
	public static List<RemarkDomain> lst;

	@SuppressWarnings("static-access")
	public RemarkAdapter(Context con,	List<RemarkDomain> lst){
		
		this.con=con;
		this.lst=lst;
		
	}

	@Override
	public int getCount() {
		return lst.size();
		
	}

	@Override
	public Object getItem(int position) {
		return lst.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View v, ViewGroup parent) {
		if(v==null)
		{
			LayoutInflater in=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v=in.inflate(R.layout.txt_display_remark, null);
			
		}
		TextView txtDate=(TextView)v.findViewById(R.id.txt_remark_date);
		TextView txtTaskName=(TextView)v.findViewById(R.id.txt_remark_task);
		TextView txtUserName=(TextView)v.findViewById(R.id.txt_remark_user);
		TextView txtState=(TextView)v.findViewById(R.id.txt_remark_state);
		TextView txtRemark=(TextView)v.findViewById(R.id.txt_remark);
		final RadioButton rb = (RadioButton)v.findViewById(R.id.radiobtnRemark);
		
		
		
		rb.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				
				for(int i=0;i<lst.size();i++)
				{
					System.out.println(position+"  **  "+i);
					if(position==i)
					{
						rb.setChecked(true);
					}
					else
					{
						rb.setChecked(false);
					}
				}
				
				
				
				
			}
		});
		
		
		
		FormatDateTime fDateTime=new FormatDateTime();
		try {
			 String date=fDateTime.getFormattedDateTime(lst.get(position).getTimeStamp());
			 txtDate.setText("Date : "+date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(":::"+lst.size()+"::::::::::"+lst.get(0).getId()+lst.get(0).getState()+lst.get(0).getTaskName()
				+lst.get(0).getText()
				+lst.get(0).getTimeStamp()
				+lst.get(0).getUserName()
				);
			
			txtUserName.setText(lst.get(position).getUserName());
			txtTaskName.setText("TaskName : "+lst.get(position).getTaskName());
			txtState.setText("State    : "+lst.get(position).getState());
			txtRemark.setText("Text     : "+lst.get(position).getText());
		
	return v;
	}
	}
