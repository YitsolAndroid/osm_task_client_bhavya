/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.reporting;

import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;


import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.restWS.server.GetFindOrder;
import com.bm.task.client.restWS.server.GetReportServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.utils.ServerUrl;
import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

public class StatusDescription extends Fragment implements OnClickListener {

	private ListView lstOrders;
	public static JSONObject json; // to accumulate chart data
	private LinearLayout llTotal, llComplete, llPending, llFailed;
	private Context context;
	protected ConnectionDetector connectionDetector;

	public StatusDescription(Context con) {
		this.context = con;
	}

	@SuppressLint({ "JavascriptInterface", "SetJavaScriptEnabled" })
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.orders, container, false);

		ViewGroup root = (ViewGroup) rootView.findViewById(R.id.ll_orders_mg);
		new FontType(context, root);

		lstOrders = (ListView) rootView.findViewById(R.id.lst_orders);

		llTotal = (LinearLayout) rootView.findViewById(R.id.ll_Total);
		llComplete = (LinearLayout) rootView.findViewById(R.id.ll_Complete);
		llPending = (LinearLayout) rootView.findViewById(R.id.ll_Pending);
		llFailed = (LinearLayout) rootView.findViewById(R.id.ll_Failed);

		LinearLayout[] ll = { llTotal, llComplete, llPending, llFailed };

		ll[0].setOnClickListener(this);
		ll[1].setOnClickListener(this);
		ll[2].setOnClickListener(this);
		ll[3].setOnClickListener(this);

		lstOrders.setAdapter(new StatusDescriptionAdapater(context,
				GetReportServer.totalList));
		
		lstOrders.setOnItemClickListener(new OnItemClickListener() {

			private Encryption encryption;

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				String orderState = StatusDescriptionAdapater.lst.get(position).getStatusName();
				
				connectionDetector = new ConnectionDetector(context);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							context, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{
				
				Toast.makeText(context, "NAME : "+orderState, Toast.LENGTH_SHORT).show();
				if(orderState.equalsIgnoreCase("All"))
					orderState="";
				
				else if(orderState.equalsIgnoreCase("completed")||orderState.equalsIgnoreCase("aborted"))
						orderState ="closed."+orderState;
				
				else if(orderState.equalsIgnoreCase("in_progress"))
					orderState = "open.running."+orderState;
				
				else if(orderState.equalsIgnoreCase("suspended")||orderState.equalsIgnoreCase("cancelled")||
						orderState.equalsIgnoreCase("failed")||orderState.equalsIgnoreCase("not_started")||
						orderState.equalsIgnoreCase("waiting_for_revision"))
					orderState = "open.not_running."+orderState;
				
				else if(orderState.equalsIgnoreCase("cancelling")||orderState.equalsIgnoreCase("amending"))
					orderState = "open.running.compensating."+orderState;
				
				encryption = new Encryption();
				try {
					String encoded = encryption.encrypt(FragmentDisplayActivity.userName + ":"
							+ FragmentDisplayActivity.password);

				new GetFindOrder(context, FragmentDisplayActivity.userName,
						FragmentDisplayActivity.password,"","", CheckConnectivity.server,CheckConnectivity.port,
						"","","","",orderState,"",
						"","","",
						"","",encoded)
						.execute(ServerUrl.serverUrlXml+"queryString");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
				
			}
		});

		return rootView;
	}

	public String load() {
		return json.toString();
	}

	public void popup(final String message) {
		((Activity) context).runOnUiThread(new Runnable() {
			public void run() {
			}
		});
	}

	public void onBackPressed() {
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.ll_Total:
			gotoOrdrs(GetReportServer.totalList);
			break;
		case R.id.ll_Complete:
			gotoOrdrs(GetReportServer.completedList);
			break;
		case R.id.ll_Pending:
			gotoOrdrs(GetReportServer.pendingList);
			break;
		case R.id.ll_Failed:
			gotoOrdrs(GetReportServer.failedList);
			break;

		default:
			break;
		}

	}

	private void gotoOrdrs(List<StatusDomain> lst) {
		lstOrders.setAdapter(new StatusDescriptionAdapater(context, lst));
	}

}
