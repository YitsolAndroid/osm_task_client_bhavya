/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import java.util.List;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Spinner;


import com.bm.task.client.domain.ServerDomain;
import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.bm.task.client.restWS.server.GetProcessServer;
import com.bm.task.client.restWS.server.GetWorkListServer;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.ConnectionDetector;
import com.bm.task.client.utils.Encryption;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.utils.NextActivity;
import com.bm.task.client.utils.ServerUrl;
import com.example.osm_xmlapi.R;

public class WorkListActivity extends Fragment{
	
	public static String selected;

	private ExpandableListView workList;
	private int lastExpandedPosition = -1;


	public static EditText edtOrderId;
	public static Button btnOk;
	private Button btnWorkList;
	private Spinner spnSelect;
	
	private String[] spinnerArray={"Editor","Change Status","Process History","Add Remark"};

	private Context context;

	protected ConnectionDetector connectionDetector;

	
	public WorkListActivity(Context context) {
		this.context = context;
	}



	@SuppressWarnings("static-access")
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.worklist, container, false);
	         
	    

		workList = (ExpandableListView) rootView.findViewById(R.id.lst_workList);

		edtOrderId = (EditText)  rootView.findViewById(R.id.edt_orderId);

		btnOk = (Button)  rootView.findViewById(R.id.btn_Go);
		btnWorkList = (Button)  rootView.findViewById(R.id.btn_workList);
		
		
		spnSelect = (Spinner) rootView.findViewById(R.id.spn_select);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		ViewGroup root = (ViewGroup)  rootView.findViewById(R.id.ll_worklist);
		new FontType(context, root);
		
		spnSelect.setAdapter(new SpnAdapter(context, spinnerArray));
		
		spnSelect.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				
				selected=SpnAdapter.lst[pos];
				System.out.println(selected);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		Encryption encryption = new Encryption();
		try {
			String encoded = encryption.encrypt(NextActivity.userName+":"+NextActivity.password);
			new GetProcessServer(context,workList,encoded).execute(ServerUrl.serverUrl +"getProcessofOrders");
		}catch(Exception e){
			
		}
		
		btnWorkList.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				connectionDetector = new ConnectionDetector(context);
				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							context, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				}else{

				Intent intent = new Intent(context,
						FragmentDisplayActivity.class);
				intent.putExtra("selected", "worklist");
				intent.putExtra("flag", 1);
				startActivity(intent);
				}

			}
		});
		
		workList.setOnGroupExpandListener(new OnGroupExpandListener() {

		    public void onGroupExpand(int groupPosition) {
		    
		            if (lastExpandedPosition != -1
		                    && groupPosition != lastExpandedPosition) {
		                workList.collapseGroup(lastExpandedPosition);
		            }
		            lastExpandedPosition = groupPosition;
		    }
		});
		


		
	    return rootView;
    

	}

	public void onBackPressed() {
		
		Intent in = new Intent(context,NextActivity.class);
		in.putExtra("user", NextActivity.userName);
		in.putExtra("password", NextActivity.password);
		startActivity(in);

		

	}


}
