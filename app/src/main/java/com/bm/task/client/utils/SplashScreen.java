package com.bm.task.client.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;

import com.example.osm_xmlapi.R;

public class SplashScreen extends Activity{
	
	
	 
	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        setContentView(R.layout.splash_screen);

			 ViewGroup root = (ViewGroup) findViewById(R.id.ll_selectPage); 
			 new FontType(getApplicationContext(), root);
	         
	// METHOD 1     
	         
	         /****** Create Thread that will sleep for 1 seconds *************/        
	        Thread background = new Thread() {
	            public void run() {
	                 
	                try {
	                    // Thread will sleep for 5 seconds
	                    sleep(5*1000);
	                     
	                    // After 5 seconds redirect to another intent
	                    Intent i=new Intent(getBaseContext(),LoginActivity.class);
	                    startActivity(i);
	                     
	                    //Remove activity
	                    finish();
	                     
	                } catch (Exception e) {
	                 
	                }
	            }
	        };
	         
	        // start thread
	        background.start();
	    }
	        
	        protected void onDestroy() {
	            
	            super.onDestroy();
	             
	        }
	         

}
