/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.utils;

/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.example.osm_xmlapi.R;

import src.main.java.com.bm.task.client.connection.CheckConnectivity;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class ChangeIP extends Activity {
	
	protected static final String PREFRENCES_NAME = "Server";
	private EditText edtIP;
	private EditText edtPort;
	private Button btnOk;
	private Button btnCancel;
	private CheckBox cbSave;
	private String server;
	private String Port;
	

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState)  {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.change_ip);
		edtIP = (EditText) findViewById(R.id.edt_ip);
		edtPort = (EditText) findViewById(R.id.edt_port);
		
		SharedPreferences settings = getSharedPreferences(PREFRENCES_NAME, 0);		
		CheckConnectivity.server = settings.getString("ip", "");
		CheckConnectivity.port = settings.getString("port", "");
		edtIP.setText(CheckConnectivity.server);
		edtPort.setText(CheckConnectivity.port);

		// Button id initialisation
		btnOk = (Button) findViewById(R.id.btn_Ok);
		btnCancel = (Button) findViewById(R.id.btn_Cancel);
		
		cbSave = (CheckBox) findViewById(R.id.chkSave);

		btnOk.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				server = edtIP.getText().toString();
				Port= edtPort.getText().toString();
				if (server.equalsIgnoreCase("") || Port.equalsIgnoreCase("")) {
					CommonPopUp commonPopUp = new CommonPopUp(ChangeIP.this,
							"Please Enter Proper IP and Port");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();

				}
			
				else 
				{
					CheckConnectivity.server=server;
					CheckConnectivity.port=Port;
					
					
					Intent intent = new Intent(ChangeIP.this,
							LoginActivity.class);
					startActivity(intent);
					
				}

			}
		});
		
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				finish();
				hidevirtualkeyboardinfragment(edtIP);
				hidevirtualkeyboardinfragment(edtPort);
				
				
			}
		});
		
		cbSave.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@SuppressLint("CommitPrefEdits")
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				
				if(isChecked == true)
				{
					server = edtIP.getText().toString();
					Port= edtPort.getText().toString();
					
					SharedPreferences settings = getSharedPreferences(PREFRENCES_NAME, 0);
					settings.edit().putString("ip", server).putString("port", Port).commit();

					/*
					 * clear data
					 * SharedPreferences settings = getSharedPreferences(
							PREFRENCES_NAME, Context.MODE_PRIVATE);
							settings.edit().clear().commit();*/

					
				
					
				}
				
			}
		});

	}
	
	
	
	/**
	 * To disable back button in device
	 */
	public void onBackPressed() {

	}
	
	public void hidevirtualkeyboardinfragment(EditText edt)
	{
		InputMethodManager imm = (InputMethodManager)ChangeIP.this.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);

	}

}
