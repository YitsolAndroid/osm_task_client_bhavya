/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.worklist;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.bm.task.client.utils.FontType;
import com.example.osm_xmlapi.R;

public class HistoryDescription extends Activity {
	
	private ListView lstHistDescription;
	private TextView taskName;
	private ProcessHistoryDomain processHistoryDomain;
	private List<ProcessHistoryDomain> histList=new ArrayList<ProcessHistoryDomain>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.history_description);
		
		ViewGroup root = (ViewGroup) findViewById(R.id.ll_histDesc);
		new FontType(getApplicationContext(), root);
		String task=getIntent().getExtras().getString("task");
		
		
		lstHistDescription = (ListView)findViewById(R.id.lst_hist_desc);
		
		taskName = (TextView)findViewById(R.id.txt_task_Heading);
		
		taskName.setText(""+task);
		
		histList.clear();
		for(int i=0;i<ProcessHistoryActivity.finalList.size();i++)
		{
			if(task.equalsIgnoreCase(ProcessHistoryActivity.finalList.get(i).getTask()))
			{
				processHistoryDomain = new ProcessHistoryDomain();
				processHistoryDomain.setTask(ProcessHistoryActivity.finalList.get(i).getTask());
				processHistoryDomain.setExpectedCompletionDate(ProcessHistoryActivity.finalList.get(i).getExpectedCompletionDate());
				processHistoryDomain.setStarted(ProcessHistoryActivity.finalList.get(i).getStarted());
				processHistoryDomain.setCompleted(ProcessHistoryActivity.finalList.get(i).getCompleted());
				processHistoryDomain.setActualDuration(ProcessHistoryActivity.finalList.get(i).getActualDuration());
				processHistoryDomain.setStatus(ProcessHistoryActivity.finalList.get(i).getStatus());
				processHistoryDomain.setState(ProcessHistoryActivity.finalList.get(i).getState());
				processHistoryDomain.setUser(ProcessHistoryActivity.finalList.get(i).getUser());
				
				histList.add(processHistoryDomain);
				
			}
		}
		lstHistDescription.setAdapter(new HistDescAdapter(HistoryDescription.this,histList));
	}
}
