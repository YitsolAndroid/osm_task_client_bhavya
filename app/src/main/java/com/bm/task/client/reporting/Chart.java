package com.bm.task.client.reporting;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bm.task.client.restWS.server.GetReportServer;
import com.bm.task.client.utils.FontType;
import com.example.osm_xmlapi.R;

public class Chart extends Fragment {
	
	private GraphicalView mChart;
	private Context context;
	private LinearLayout chartContainer;
	
	public Chart(Context con)
	{
		this.context = con;
		
	}
	
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.chart, container, false);
		
		ViewGroup root = (ViewGroup) rootView.findViewById(R.id.ll_chart);
		new FontType(context, root);
		
		chartContainer = (LinearLayout) rootView.findViewById(R.id.chart_container);
		
        openChart();
		return rootView;
	}

	private void openChart() {
		
		// Pie Chart Section Names
				String[] code = new String[] { "Total Orders", "Completed Orders",
						"Pending Orders", "Failed Orders" };

				// Pie Chart Section Value
				double[] distribution = { Double.parseDouble(String.valueOf(GetReportServer.total)),
						Double.parseDouble(String.valueOf(GetReportServer.completed)),
						Double.parseDouble(String.valueOf(GetReportServer.pending)),
						Double.parseDouble(String.valueOf(GetReportServer.failed)) };

				// Color of each Pie Chart Sections
				int[] colors = { Color.parseColor("#3090f0"), Color.parseColor("#ec6464"), Color.parseColor("#98df58"), Color.parseColor("#24dcd4")};

				// Instantiating CategorySeries to plot Pie Chart
				CategorySeries distributionSeries = new CategorySeries("");
				for (int i = 0; i < distribution.length; i++) {
					// Adding a slice with its values and name to the Pie Chart
					distributionSeries.add(code[i], distribution[i]);
				}
				// Instantiating a renderer for the Pie Chart
				
				
				
				DefaultRenderer defaultRenderer = new DefaultRenderer();
				for (int i = 0; i < distribution.length; i++) {
					SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
					seriesRenderer.setColor(colors[i]);
					seriesRenderer.setDisplayChartValues(true);
					// Adding a renderer for a slice
					defaultRenderer.addSeriesRenderer(seriesRenderer);
				}

				defaultRenderer.setApplyBackgroundColor(true);
			//	defaultRenderer.setBackgroundColor(Color.parseColor("#157DEC"));	
				defaultRenderer.setLabelsColor(Color.BLACK);
				defaultRenderer.setLabelsTextSize(15);
				defaultRenderer.setScale((float) 0.75);
				defaultRenderer.setChartTitle("OSM REPORTS");
				defaultRenderer.setChartTitleTextSize(25);
				defaultRenderer.setLegendTextSize(15);
				defaultRenderer.setPanEnabled(false);
				defaultRenderer.setDisplayValues(true);
				defaultRenderer.isShowAxes();
				
						
				mChart = (GraphicalView) ChartFactory.getPieChartView(context, distributionSeries, defaultRenderer);
				chartContainer.addView(mChart);
	}
}
