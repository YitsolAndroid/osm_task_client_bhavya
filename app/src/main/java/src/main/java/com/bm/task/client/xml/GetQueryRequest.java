package src.main.java.com.bm.task.client.xml;


import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import src.main.java.com.bm.task.client.connection.OSM_Query;
import src.main.java.com.bm.task.client.xml.XMLParser;


public class GetQueryRequest {

	private String userName;
	private String password;
	public static String xmlFile;// Xml file from Client
	private XMLParser xmlParser;
	private Document doc;
	protected static final String KEY_ITEM_Task = "Task";
	private ArrayList<String> lst = new ArrayList<String>();

	public GetQueryRequest(String userName, String password) {

		this.userName = userName;
		this.password = password;

	}

	@SuppressWarnings("static-access")
	public ArrayList<String> getTask() {
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password = password;
		osm_Query.requestXml = "<TaskDescription.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
				+ "<Namespace>OSM7BroadbandDSL</Namespace>"
				+ "<Version>1.0.0</Version>" + "</TaskDescription.Request>";

		try {
			osm_Query.getQueryData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		StringBuffer res = osm_Query.xmlResponse;

		xmlFile = res.toString();

		xmlParser = new XMLParser();
		doc = xmlParser.getDomElement(xmlFile);
		
		NodeList task = doc.getElementsByTagName(KEY_ITEM_Task);
		for(int i=0;i<task.getLength();i++)
		{
			if(i==0)
			{
				lst.add("All");
			}
			Element ele = (Element) task.item(i);
			String taskDesc = ele.getAttribute("desc");
			System.out.println("My Desc : "+taskDesc);
			lst.add(taskDesc);
		}
		
		return lst;
	}

}
