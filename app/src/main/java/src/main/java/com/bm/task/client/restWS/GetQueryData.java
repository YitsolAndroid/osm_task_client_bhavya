/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.restWS;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bm.task.client.xml.WorkListDomain;

import src.main.java.com.bm.task.client.xml.XMLParser;


/**
 * Getting Worklist Values
 * @author Bhavya
 *
 */
public class GetQueryData {


	public static int flag = 0;

	private String KEY_ITEM = "Orderdata";
	private String id = "_order_seq_id";
	private String orderState = "_current_order_state";
	private String state = "_order_state";
	private String source = "_order_source";
	private String ref_no = "_reference_number";
	private String nameSpace = "_namespace";
	private String expectedOrderCompletionDate = "_compl_date_expected";
	private String createdDate = "_date_pos_started";
	private String type = "_order_type";
	private String priority = "_priority";
	private String version = "_version";
	private String started = "_date_pos_started";
	private String executionMode = "_execution_mode";
	private String task = "_task_id";
	private String user = "_user";
	private String num_remarks = "_num_remarks";
	
	

	private List<WorkListDomain> lst = new ArrayList<WorkListDomain>();
	private WorkListDomain workListDomain;
	private String xml;

	public GetQueryData(String xml) {
		this.xml = xml;
	}

	public List<WorkListDomain> getQueryList() {
		
		if (xml.equalsIgnoreCase("Failure")) {
			flag = 1;
			

		} else {
			

			XMLParser xmlParser = new XMLParser();
			Document doc = xmlParser.getDomElement(xml);
			NodeList nl = doc.getElementsByTagName(KEY_ITEM);

			for (int position = 0; position < nl.getLength(); position++) {
				Element e = (Element) nl.item(position);

				workListDomain = new WorkListDomain();
				workListDomain.setSeqIds(xmlParser.getValue(e, id));
				workListDomain.setRef_no(xmlParser.getValue(e, ref_no));
				workListDomain.setSource(xmlParser.getValue(e, source));
				workListDomain.setOrderState(xmlParser.getValue(e, state));
				workListDomain.setType(xmlParser.getValue(e, type));
				workListDomain.setNameSpace(xmlParser.getValue(e, nameSpace));
				workListDomain.setVersion(xmlParser.getValue(e, version));
				workListDomain.setPriority(xmlParser.getValue(e, priority));
				workListDomain.setCreatedDate(xmlParser.getValue(e, createdDate));
				workListDomain.setTask(xmlParser.getValue(e, task));
				workListDomain.setCurrentOrderState(xmlParser.getValue(e,orderState));
				workListDomain.setExecutionMode(xmlParser.getValue(e, executionMode));
				workListDomain.setStartDate(xmlParser.getValue(e, started));
				workListDomain.setUser(xmlParser.getValue(e, user));
				workListDomain.setNumRemarks(xmlParser.getValue(e, num_remarks));
				workListDomain.setExpectedOrderCompletionDate(xmlParser.getValue(e, expectedOrderCompletionDate));
				lst.add(workListDomain);

			}

		}
		return lst;
	}

}
