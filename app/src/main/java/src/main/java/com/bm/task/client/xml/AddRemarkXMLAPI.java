package src.main.java.com.bm.task.client.xml;


import src.main.java.com.bm.task.client.connection.OSM_Query;

public class AddRemarkXMLAPI {
	
	private String userName;
	private String password;


	public AddRemarkXMLAPI(String userName, String password) {

		this.userName = userName;
		this.password = password;

	}
	
	public void addRemark(String id,String remark,String attachment)
	{
	OSM_Query osm_Query = new OSM_Query();
	OSM_Query.user = userName;
	OSM_Query.password = password;

	try {
		String request1 = "<UpdateOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
		String request2 = "<OrderID>" + id + "</OrderID>";
		String request3 = "<View>BroadbandDSLOrderCreation</View>";
		String request4 = "<AddRemark> <Text>"+remark+"</Text>";
		String request5 = "<Attachments>"+attachment+"</Attachments></AddRemark></UpdateOrder.Request>";
		OSM_Query.requestXml = request1 + request2 + request3 + request4
				+ request5;
		osm_Query.getQueryData();
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	

}
