/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package src.main.java.com.bm.task.client.connection;

import java.io.BufferedReader;


import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Login and Logout from Session are defined here
 * @author Bhavya
 *
 */

public class LoginDetails {
	
	public static String cookie;
	public static StringBuffer response;
	public static String strResponse="true";
	public static StringBuffer xmlResponse;

	public static String user, password;
	 String localIP;
	 String port;
	 public static int loginSuccess=1;
	 public String Result;
	 public static String orderId,HistOrderId,status;
	
	
	@SuppressWarnings("static-access")
	public String getLoginData() throws Exception {

		@SuppressWarnings("deprecation")
		String str = URLEncoder.encode("username") + "="
				+ URLEncoder.encode(user) + "&" + URLEncoder.encode("password")
				+ "=" + URLEncoder.encode(password);
		CheckConnectivity checkConnectivity=new CheckConnectivity();
		boolean connectivity=checkConnectivity.Connectivity();
		System.out.println("My answer"+connectivity);
		if(connectivity==true)
		{
		Result="Success";
		localIP=checkConnectivity.server;
		port=checkConnectivity.port;
		byte[] bytes = str.getBytes();
		URL url = new URL("http://" + localIP
				+ ":"+port+"/OrderManagement/XMLAPI/login");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(1000);
		connection.setAllowUserInteraction(false);
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length",
				String.valueOf(bytes.length));
		connection.connect();
		OutputStream out = connection.getOutputStream();
		out.write(bytes);
		out.close();
		
		try{
		int code = connection.getResponseCode();
		System.out.println("Code output : "+code);

		if (code == HttpURLConnection.HTTP_OK) {
			loginSuccess=1;
			String receivedcookie = connection.getHeaderField("Set-Cookie");
			if (receivedcookie == null) {
				loginSuccess=2;
				throw new Exception("Server did not return session cookie");
			}
			cookie = receivedcookie.substring(0, receivedcookie.indexOf(';'));
		} else {
			loginSuccess=0;
			System.out.println("Gettig Here");
			throw new Exception("HTTP response code != 200 OK :" + code);
		}
		
		if(loginSuccess==1)
		{
		
		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		String inputLine;
		response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		
		System.out.println(response);
		strResponse=response.toString();
		in.close();
		}
		}catch (Exception e){
			loginSuccess=0;
		}
		connection.disconnect();
		
		}
		
		return strResponse;
	}
	
	
	@SuppressWarnings({ "static-access", "unused" })
	public String getLogoutData() throws Exception {
		
		@SuppressWarnings("deprecation")
		String str = URLEncoder.encode("username") + "="
				+ URLEncoder.encode(user) + "&" + URLEncoder.encode("password")
				+ "=" + URLEncoder.encode(password);
		CheckConnectivity checkConnectivity=new CheckConnectivity();
		boolean connectivity=checkConnectivity.Connectivity();
		System.out.println("My answer"+connectivity);
		if(connectivity==true)
		{
		Result="Success";
		localIP=checkConnectivity.server;
		port=checkConnectivity.port;
		byte[] bytes = str.getBytes();
		URL url = new URL("http://" + localIP
				+ ":"+port+"/OrderManagement/XMLAPI/logout");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection = (HttpURLConnection) url.openConnection();
		connection.setAllowUserInteraction(false);
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod("POST");
		connection.addRequestProperty("Cookie", cookie);
		connection.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length", "0");
		connection.connect();
		int code = connection.getResponseCode();

		if (code == HttpURLConnection.HTTP_OK) {

		} else {
			loginSuccess=0;
			//throw new Exception("HTTP response code != 200 OK :" + code);
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		
		String inputLine;
		response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		System.out.println(response);
		in.close();
		connection.disconnect();
		}
		
		return response.toString();
	}
	

}
