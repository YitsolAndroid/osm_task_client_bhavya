/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package src.main.java.com.bm.task.client.connection;

import java.net.HttpURLConnection;
import java.net.URL;

public class CheckConnectivity {
	
	/*public static String publicIP="192.168.2.13";
	public static String port="7003";*/
	public static String server = "";
	public static String port ="";
	
	
	public static Boolean result;
	
	
	public Boolean Connectivity() throws Exception {
		
		URL url = new URL(
				"http://"+CheckConnectivity.server+":"+CheckConnectivity.port+"/OrderManagement/");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	
		try
		{
		connection.setConnectTimeout(1000);

		if (connection.getHeaderField(0) == null) {
			result = false;
		} else {
			result = true;
		}
		}
		catch(Exception e){
			result = false;
		}

		return result;
	}

}
