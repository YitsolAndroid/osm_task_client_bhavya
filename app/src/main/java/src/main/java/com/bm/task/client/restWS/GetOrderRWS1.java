package com.bm.task.client.restWS;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bm.task.client.xml.WorkListDomain;

import src.main.java.com.bm.task.client.xml.XMLParser;

public class GetOrderRWS1 {

	private static final String KEY_ITEM_Error = "status";
	private static final String KEY_ITEM_Error1 = "GetOrder.Error";
	private String xmlOutput;
	private Element parentElement;
	private List<WorkListDomain> taskList = new ArrayList<WorkListDomain>();
	private List<WorkListDomain> lst = new ArrayList<WorkListDomain>();
	private WorkListDomain workListDomain;
	public static int flag;

	static final String KEY_ITEM_COMPLETE = "Completed";
	public static NodeList nlStatus;
	public static List<String> statusList = new ArrayList<String>();

	public GetOrderRWS1(String xmlOutput) {
		this.xmlOutput = xmlOutput;
	
	}

	public List<WorkListDomain> getOrderRWS() {

		XMLParser xmlParser = new XMLParser();
		Document doc = xmlParser.getDomElement(xmlOutput);

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		NodeList error1 = doc.getElementsByTagName(KEY_ITEM_Error1);
		if (error.getLength() == 1 || error1.getLength() == 1) {
			flag = 1;
		} else {
			flag = 0;
			NodeList nl = doc.getElementsByTagName("_root");

			System.out.println(nl.getLength());

			for (int i = 0; i < nl.getLength(); i++) {
				String nodeName = nl.item(i).getNodeName();
				NodeList nlParent = doc.getElementsByTagName(nodeName);
				parentElement = (Element) nlParent.item(0);
				if (nodeName.equalsIgnoreCase("_root")) {
					lst = xmlParser.gotoXmlParse(parentElement);
				}
			}

			for (int i = 0; i < lst.size(); i++) {
				if (i > 1) {
					if (lst.get(i).getNodeName().equalsIgnoreCase("index")) {

					} else {
						System.out.println(lst.get(i).getNodeName());
						workListDomain = new WorkListDomain();
						workListDomain.setNodeName(lst.get(i).getNodeName());
						workListDomain.setNodeValue(lst.get(i).getNodeValue());
						taskList.add(workListDomain);
					}
				}
			}
		}

		

		return taskList;

	}
}
