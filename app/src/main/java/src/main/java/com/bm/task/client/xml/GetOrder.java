/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package src.main.java.com.bm.task.client.xml;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import src.main.java.com.bm.task.client.connection.OSM_Query;


/**
 * 
 * @author Bhavya
 * 
 */
public class GetOrder {

	private String userName;
	private String password;

	private XMLParser xmlParser;
	private com.bm.task.client.xml.GetHistId getHistId;

	protected static final String KEY_ITEM_TaskNStatus = "TaskStatesNStatuses";
	static final String KEY_ITEM_COMPLETE = "Completed";
	static final String KEY_ITEM_HistID = "OrderHistID";

	public static String xmlFileForGetorder;// Xml file from Client
	protected static final String KEY_ITEM_Error = "Error";
	private Document doc;
	private Element parentElement;
	public static NodeList nlStatus;

	List<com.bm.task.client.xml.WorkListDomain> lst = new ArrayList<com.bm.task.client.xml.WorkListDomain>();
	List<com.bm.task.client.xml.WorkListDomain> taskList = new ArrayList<com.bm.task.client.xml.WorkListDomain>();
	public static List<String> statusList = new ArrayList<String>();
	com.bm.task.client.xml.WorkListDomain workListDomain;

	String histId;
	public static int flag;
	private String taskName;

	public GetOrder(String userName, String password, String taskName) {

		this.userName = userName;
		this.password = password;
		this.taskName = taskName;

	}

	@SuppressWarnings("static-access")
	public List<com.bm.task.client.xml.WorkListDomain> getOrder(String id) {
		getHistId = new com.bm.task.client.xml.GetHistId(userName, password, taskName);
		histId = getHistId.getHistoryID(id);
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password = password;

		try {
			String request1 = "<GetOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
			String request2 = "<OrderID>" + id + "</OrderID>";
			String request3 = "<Accept>true</Accept>";
			String request4 = "<OrderHistID>" + histId + "</OrderHistID>";
			String request5 = "</GetOrder.Request>";
			osm_Query.requestXml = request1 + request2 + request3 + request4
					+ request5;
			osm_Query.getQueryData();
		} catch (Exception e) {
			e.printStackTrace();
		}

		StringBuffer resForGetOrder = osm_Query.xmlResponse;

		xmlFileForGetorder = resForGetOrder.toString();

		xmlParser = new XMLParser();
		doc = xmlParser.getDomElement(xmlFileForGetorder);

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		if (error.getLength() == 1) {
			flag = 1;
		} else {
			flag = 0;
			Element st = doc.getDocumentElement();
			NodeList nl = st.getChildNodes();
			for (int i = 0; i < nl.getLength(); i++) {
				String nodeName = nl.item(i).getNodeName();
				NodeList nlParent = doc.getElementsByTagName(nodeName);
				parentElement = (Element) nlParent.item(0);
				String nodeValue = xmlParser.getElementValue(parentElement);
				workListDomain = new com.bm.task.client.xml.WorkListDomain();
				workListDomain.setNodeValue(nodeValue);
				workListDomain.setNodeName(nodeName);
				lst.add(workListDomain);

				if (nodeName.equalsIgnoreCase("_root")) {
					taskList = xmlParser.gotoXmlParse(parentElement);
				}

			}

			NodeList nlParent = doc.getElementsByTagName(KEY_ITEM_HistID);
			parentElement = (Element) nlParent.item(0);
			histId = xmlParser.getElementValue(parentElement);

			try {
				String request1 = "<ListStatesNStatuses.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
				String request2 = "<OrderID>" + id + "</OrderID>";
				String request4 = "<OrderHistID>" + histId + "</OrderHistID>";
				String request5 = "</ListStatesNStatuses.Request>";
				osm_Query.requestXml = request1 + request2 + request4
						+ request5;
				osm_Query.getQueryData();
				;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			String statusStr = osm_Query.xmlResponse.toString();
			xmlParser = new XMLParser();
			doc = xmlParser.getDomElement(statusStr);

			nlStatus = doc.getElementsByTagName(KEY_ITEM_COMPLETE);
			Element eleStatus = (Element) nlStatus.item(0);
			nlStatus = eleStatus.getChildNodes();
			statusList.clear();
			for (int i = 0; i < nlStatus.getLength(); i++) {
				String status = nlStatus.item(i).getNodeName();
				statusList.add(status);
			}
		}
		return taskList;
	}

}
