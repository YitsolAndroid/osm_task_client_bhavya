/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package src.main.java.com.bm.task.client.xml;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Getting users and state of task are defined here
 * 
 * @author Bhavya
 * 
 */

public class GetListStatus {

	private XMLParser xmlParser;

	private Element parentElement;

	protected static final String KEY_ITEM_Error = "Error";
	protected static final String KEY_ITEM_TaskNStatus = "TaskStatesNStatuses";
	static final String KEY_ITEM_COMPLETE = "Completed";
	static final String KEY_ITEM_ASSIGNED = "User";
	static final String state = "State";

	public static List<String> nodeNameList = new ArrayList<String>();
	public static ArrayList<String> childlst = new ArrayList<String>();
	public static ArrayList<ArrayList<String>> lst = new ArrayList<ArrayList<String>>();

	public static NodeList nlTasks;

	public static int flag = 0;

	public GetListStatus() {

	}

	/**
	 * List of the status, user and task state
	 * 
	 * @param xml
	 */
	public List<String> getStatus(String xml) {

		nodeNameList.clear();
		lst.clear();

		xmlParser = new XMLParser();
		Document doc = xmlParser.getDomElement(xml);
		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		if (error.getLength() == 1) {
			flag = 1;

		} else {
			flag = 0;
			System.out.println("myanwe" + xml);

			nlTasks = doc.getElementsByTagName(KEY_ITEM_TaskNStatus);

			Element ele = (Element) nlTasks.item(0);
			NodeList nl = ele.getChildNodes();

			for (int i = 0; i < nl.getLength(); i++) {
				nodeNameList.add(nl.item(i).getNodeName());

				NodeList nlParent = doc.getElementsByTagName(nodeNameList
						.get(i));
				parentElement = (Element) nlParent.item(0);
				NodeList nlChild = parentElement.getChildNodes();

				childlst = new ArrayList<String>();
				if (nlChild.getLength() == 0) {
					childlst.add("");
				} else {
					for (int j = 0; j < nlChild.getLength(); j++) {
						String nodeName = nlChild.item(j).getNodeName();
						NodeList nodeList = parentElement
								.getElementsByTagName(nodeName);
						Element ele1 = (Element) nodeList.item(j);
						String nodeValue = xmlParser.getElementValue(ele1);

						if (nodeValue.equalsIgnoreCase("")) {

							childlst.add(nodeName);
						} else {
							childlst.add(nodeValue);
						}

					}

				}

				lst.add(childlst);

			}

		}

		return nodeNameList;
	}

}
