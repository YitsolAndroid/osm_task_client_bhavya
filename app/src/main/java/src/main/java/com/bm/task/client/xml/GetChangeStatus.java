package src.main.java.com.bm.task.client.xml;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import src.main.java.com.bm.task.client.connection.OSM_Query;


/**
 * Receive Order,AssignOrder ,Complete Order and Update Order XMLAPI are Defined 
 * @author Bhavya
 *
 */
public class GetChangeStatus {

	private final String userName;
	private final String password;
	
	private final String taskName;
	
	private String histID;
	
	public static String xmlFile;// Xml file from Client
	
	private XMLParser xmlParser;
	
	private Document doc;
	
	protected static final String KEY_ITEM_Error = "Error";
	
	public static int flag=0;
	
	public GetChangeStatus(String userName,String password,String taskName){
		
		this.userName=userName;
		this.password=password;
		this.taskName=taskName;
		
	}
	
	/**
	 * to change status from accept to receive
	 * @param id
	 */

	@SuppressWarnings("static-access")
	public void getReceiveOrder(String id) {
		
		com.bm.task.client.xml.GetHistId getHistId = new com.bm.task.client.xml.GetHistId(userName,password,taskName);
		histID = getHistId.getHistoryID(id);
		
		System.out.println(histID);
		
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password = password;
		osm_Query.requestXml = "<ReceiveOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
				+ "<OrderID>"
				+ id
				+ "</OrderID>"
				+ "<OrderHistID>"
				+ histID
				+ "</OrderHistID>" + "</ReceiveOrder.Request>";

		try {
			osm_Query.getQueryData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		StringBuffer res = osm_Query.xmlResponse;

		xmlFile = res.toString();

		xmlParser = new XMLParser();
		doc = xmlParser.getDomElement(xmlFile);

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		if (error.getLength() == 1) {
			flag = 1;
		}
		else
		{
			flag = 0;
		}
	}

	/**
	 * assign order to the user
	 * @param id
	 * @param user
	 */
	@SuppressWarnings("static-access")
	public void getAssignOrder(String id, String user) {
		
		com.bm.task.client.xml.GetHistId getHistId = new com.bm.task.client.xml.GetHistId(userName,password,taskName);
		histID = getHistId.getHistoryID(id);
		
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password = password;
		osm_Query.requestXml = "<AssignOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
				+ "<OrderID>"
				+ id
				+ "</OrderID>"
				+ "<OrderHistID>"
				+ histID
				+ "</OrderHistID>"
				+ "<User>"
				+ user
				+ "</User>"
				+ "</AssignOrder.Request>";

		try {
			osm_Query.getQueryData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		StringBuffer res = osm_Query.xmlResponse;

		xmlFile = res.toString();

		xmlParser = new XMLParser();
		doc = xmlParser.getDomElement(xmlFile);

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		if (error.getLength() == 1) {
			flag = 1;
		}
		else
		{
			flag = 0;
		}
	}

	/**
	 * complete the order task
	 * @param id
	 * @param status
	 */
	@SuppressWarnings("static-access")
	public void getCompleteOrder(String id,String status)
	{
		com.bm.task.client.xml.GetHistId getHistId = new com.bm.task.client.xml.GetHistId(userName,password,taskName);
		histID = getHistId.getHistoryID(id);
		
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password =password;
		osm_Query.requestXml = "<CompleteOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
								+"<OrderID>"+id+"</OrderID>"
								+"<OrderHistID>"+histID+"</OrderHistID>"
								+"<Status>"+status+"</Status>"
								+"</CompleteOrder.Request>";

		try {
			osm_Query.getQueryData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		StringBuffer res = osm_Query.xmlResponse;

		xmlFile = res.toString();

		xmlParser = new XMLParser();
		doc = xmlParser.getDomElement(xmlFile);

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		if (error.getLength() == 1) {
			flag = 1;
		}
		else
		{
			flag = 0;
		}
		
	}
	
	@SuppressWarnings("static-access")
	public void getUpdateOrder(String id,String optionalValue,String status)
	{

		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password =password;

		osm_Query.requestXml = "<UpdateOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
				+ "<OrderID>"
				+ id
				+ "</OrderID>"
				+ "<View>"
				+ "BroadbandDSLOrderCreation"
				+ "</View>"
				+ "<UpdatedNodes>"
				+ optionalValue
				+ "</UpdatedNodes></UpdateOrder.Request>";

		try {
			osm_Query.getQueryData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		StringBuffer res = osm_Query.xmlResponse;

		xmlFile = res.toString();

		xmlParser = new XMLParser();
		doc = xmlParser.getDomElement(xmlFile);

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		if (error.getLength() == 1) {
			flag = 1;
		}
		else
		{
			flag = 0;
		}
		
		
		com.bm.task.client.xml.GetHistId getHistId = new com.bm.task.client.xml.GetHistId(userName,password,taskName);
		histID = getHistId.getHistoryID(id);
		
		osm_Query.requestXml = "<CompleteOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
								+"<OrderID>"+id+"</OrderID>"
								+"<OrderHistID>"+histID+"</OrderHistID>"
								+"<Status>"+status+"</Status>"
								+"</CompleteOrder.Request>";

		try {
			osm_Query.getQueryData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}

}
