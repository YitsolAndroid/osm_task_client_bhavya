/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package src.main.java.com.bm.task.client.xml;


import src.main.java.com.bm.task.client.connection.OSM_Query;

/**
 * To Resume ,Cancel and Suspend Order API are defined in this classs
 * @author Bhavya
 *
 */

public class GetChangeOrderState {
	
	private final String userName;
	private final String password;
	
	public GetChangeOrderState(String userName,String password){
		
		this.userName=userName;
		this.password=password;
		
	}
	
	@SuppressWarnings("static-access")
	/**
	 * To suspend the order
	 * @param id
	 * @param reason
	 */
	public void getSuspendOrder(String id,String reason)
	{
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password = password;
		osm_Query.requestXml = "<SuspendOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
				+ "<OrderID>"
				+ id
				+ "</OrderID>"
				+ "<Infinite/>"
				+ "<Reason>"
				+ reason
				+ "</Reason>"
				+ "</SuspendOrder.Request>";

		try {
			osm_Query.getQueryData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * To resume the suspended order
	 * @param id
	 * @param reason
	 */
	
	@SuppressWarnings("static-access")
	public void getResumedOrder(String id,String reason)
	{
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password = password;
		osm_Query.requestXml = "<ResumeOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
				+ "<OrderID>"
				+ id
				+ "</OrderID>"
				+ "<Infinite/>"
				+ "<Reason>"
				+ reason
				+ "</Reason>" 
				+ "</ResumeOrder.Request>";

		try {
			osm_Query.getQueryData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * to cancel the order
	 * @param id
	 * @param reason
	 */
	@SuppressWarnings("static-access")
	public void getCancelOrder(String id,String reason)
	{
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password = password;
		osm_Query.requestXml = "<CancelOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
				+ "<OrderID>"
				+ id
				+ "</OrderID>"
				+ "<Infinite/>"
				+ "<Reason>"
				+ reason
				+ "</Reason>" 
				+ "</CancelOrder.Request>";

		try {
			osm_Query.getQueryData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
