/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.restWS;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bm.task.client.xml.WorkListDomain;

import src.main.java.com.bm.task.client.xml.XMLParser;


/**
 * Getting Worklist Values
 * @author Bhavya
 *
 */
public class GetWorkListRWS {


	public static int flag = 0;

	static final String KEY_ITEM = "Orderdata";
	static final String seqId = "_order_seq_id";
	static final String histSeqId = "_order_hist_seq_id";
	static final String orderState = "_order_state";
	static final String executionMode = "_execution_mode";
	static final String source = "_order_source";
	static final String task = "_task_id";
	static final String ref_no = "_reference_number";
	static final String nameSpace = "_namespace";
	static final String version = "_version";
	static final String current_order_state = "_current_order_state";
	static final String user = "_user";
	static final String type = "_order_type";
	static final String priority = "_priority";
	static final String process_status = "_process_status";
	static final String num_remarks = "_num_remarks";
	private String error = "status";
	

	private List<WorkListDomain> lst = new ArrayList<WorkListDomain>();
	private WorkListDomain workListDomain;
	private String xml;

	public GetWorkListRWS(String xml) {
		this.xml = xml;
	}

	public List<WorkListDomain> getWorkList() {
		XMLParser xmlParser = new XMLParser();
		Document doc = xmlParser.getDomElement(xml);
		NodeList nlerror = doc.getElementsByTagName(error);
		System.out.println(nlerror.getLength());
		
		if (nlerror.getLength()==1) {
			flag = 1;
			

		} else {
			flag = 0;

			xmlParser = new XMLParser();
			doc = xmlParser.getDomElement(xml);
			NodeList nl = doc.getElementsByTagName(KEY_ITEM);

			for (int position = 0; position < nl.getLength(); position++) {
				Element e = (Element) nl.item(position);

				workListDomain = new WorkListDomain();
				workListDomain.setSeqIds(xmlParser.getValue(e, seqId));
				workListDomain.setHistSeqIds(xmlParser.getValue(e, histSeqId));
				workListDomain.setRef_no(xmlParser.getValue(e, ref_no));
				workListDomain.setSource(xmlParser.getValue(e, source));
				workListDomain.setOrderState(xmlParser.getValue(e, orderState));
				workListDomain.setExecutionMode(xmlParser.getValue(e,
						executionMode));
				workListDomain.setType(xmlParser.getValue(e, type));
				workListDomain.setTask(xmlParser.getValue(e, task));
				workListDomain.setNameSpace(xmlParser.getValue(e, nameSpace));
				workListDomain.setVersion(xmlParser.getValue(e, version));
				workListDomain.setPriority(xmlParser.getValue(e, priority));
				workListDomain.setCurrentOrderState(xmlParser.getValue(e,
						current_order_state));
				workListDomain.setProcessStatus(xmlParser.getValue(e,
						process_status));
				workListDomain.setUser(xmlParser.getValue(e, user));
				workListDomain.setNumRemarks(xmlParser.getValue(e, num_remarks));

				lst.add(workListDomain);

			}

		}
		return lst;
	}

}
