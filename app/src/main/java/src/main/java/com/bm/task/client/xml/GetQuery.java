package src.main.java.com.bm.task.client.xml;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import src.main.java.com.bm.task.client.connection.OSM_Query;


public class GetQuery {

	private String userName;
	private String password;

	private String result;
	public static int flag = 0;
	private String xmlFile;

	static final String KEY_ITEM = "Orderdata";
	static final String seqId = "_order_seq_id";
	static final String histSeqId = "_order_hist_seq_id";
	static final String orderState = "_order_state";
	static final String executionMode = "_execution_mode";
	static final String source = "_order_source";
	static final String task = "_task_id";
	static final String ref_no = "_reference_number";
	static final String nameSpace = "_namespace";
	static final String version = "_version";
	static final String current_order_state = "_current_order_state";
	static final String user = "_user";
	static final String type = "_order_type";
	static final String priority = "_priority";
	static final String process_status = "_process_status";
	

	private List<com.bm.task.client.xml.WorkListDomain> lst = new ArrayList<com.bm.task.client.xml.WorkListDomain>();
	private com.bm.task.client.xml.WorkListDomain workListDomain;

	public GetQuery(String userName, String password) {

		this.userName = userName;
		this.password = password;

	}

	@SuppressWarnings("static-access")
	public List<com.bm.task.client.xml.WorkListDomain> queryForOrderID(String request) {
		
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password = password;
		
		osm_Query.requestXml = "<Query.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"+request+
				"</Query.Request>";

		try {
			osm_Query.getQueryData();
			result = osm_Query.getQueryData();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		lst =getResonpseData(osm_Query,result);
		
		return lst;
		
		}

	private List<com.bm.task.client.xml.WorkListDomain> getResonpseData(OSM_Query osm_Query,
																		String result) {
		
		if (result.equalsIgnoreCase("Failure")) {
			flag = 1;
			

		} else {
			@SuppressWarnings("static-access")
			StringBuffer resForWorklist = osm_Query.xmlResponse;
			xmlFile = resForWorklist.toString();

			XMLParser xmlParser = new XMLParser();
			Document doc = xmlParser.getDomElement(xmlFile);
			NodeList nl = doc.getElementsByTagName(KEY_ITEM);

			for (int position = 0; position < nl.getLength(); position++) {
				Element e = (Element) nl.item(position);

				workListDomain = new com.bm.task.client.xml.WorkListDomain();
				workListDomain.setSeqIds(xmlParser.getValue(e, seqId));
				workListDomain.setHistSeqIds(xmlParser.getValue(e, histSeqId));
				workListDomain.setRef_no(xmlParser.getValue(e, ref_no));
				workListDomain.setSource(xmlParser.getValue(e, source));
				workListDomain.setOrderState(xmlParser.getValue(e, orderState));
				workListDomain.setExecutionMode(xmlParser.getValue(e,
						executionMode));
				workListDomain.setType(xmlParser.getValue(e, type));
				workListDomain.setTask(xmlParser.getValue(e, task));
				workListDomain.setNameSpace(xmlParser.getValue(e, nameSpace));
				workListDomain.setVersion(xmlParser.getValue(e, version));
				workListDomain.setPriority(xmlParser.getValue(e, priority));
				workListDomain.setCurrentOrderState(xmlParser.getValue(e,
						current_order_state));
				workListDomain.setProcessStatus(xmlParser.getValue(e,
						process_status));
				workListDomain.setUser(xmlParser.getValue(e, user));

				lst.add(workListDomain);

			}

		}
		return lst;
	}

	
	}