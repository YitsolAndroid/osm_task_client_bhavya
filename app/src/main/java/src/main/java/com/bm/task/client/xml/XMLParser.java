/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package src.main.java.com.bm.task.client.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


/**
 * Parsing XML for our Customization
 * @author Bhavya
 *
 */
public class XMLParser {
	
	com.bm.task.client.xml.WorkListDomain workListDomain;
	List<com.bm.task.client.xml.WorkListDomain> lst=new ArrayList<com.bm.task.client.xml.WorkListDomain>();
	
	@SuppressWarnings("unused")
	private final static Logger LOGGER = Logger.getLogger(XMLParser.class.getName());

	// constructor
	public XMLParser() {

	}
	/*** Getting XML DOM element @param XML string * */
	public Document getDomElement(String xml){
		Document doc = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {

			DocumentBuilder db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
		        is.setCharacterStream(new StringReader(xml));
		        doc = db.parse(is); 

			} catch (ParserConfigurationException e) {
				e.printStackTrace();
				return null;
			} catch (SAXException e) {
				e.printStackTrace();
	            return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}

	        return doc;
	}
	
	/** Getting node value
	  * @param elem element
	  */
	 public final String getElementValue( Node elem ) {
	     Node child;
	     if( elem != null){
	         if (elem.hasChildNodes()){
	             for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() ){
	                 if( child.getNodeType() == Node.TEXT_NODE  ){
	                     return child.getNodeValue();
	                 }
	             }
	         }
	     }
	     return "";
	 }
	 
	 /**
	  * Getting node value
	  * @param Element node
	  * @param key string
	  * */
	 public String getValue(Element e, String str) {		
			NodeList n = e.getElementsByTagName(str);		
			return this.getElementValue(n.item(0));
		}
	 
		public List<com.bm.task.client.xml.WorkListDomain> gotoXmlParse(Element element) {
			NodeList nl = element.getChildNodes();
			String nodeValue = null;
			String nodeName = null;

			for (int i = 0; i < nl.getLength(); i++) {
				nodeName = nl.item(i).getNodeName();
				NodeList nlChild = element.getElementsByTagName(nodeName);
				Element ele = (Element) nlChild.item(0);
				nodeValue = this.getElementValue(ele);

				if (nodeValue.equalsIgnoreCase("")) {
					workListDomain = new com.bm.task.client.xml.WorkListDomain();
					workListDomain.setNodeValue(nodeValue);
					workListDomain.setNodeName(nodeName);
					lst.add(workListDomain);
					gotoXmlParse(ele);

				} else {
					workListDomain = new com.bm.task.client.xml.WorkListDomain();
					workListDomain.setNodeValue(nodeValue);
					workListDomain.setNodeName(nodeName);
					lst.add(workListDomain);

				}

			}
			
			return lst;

		}
}
