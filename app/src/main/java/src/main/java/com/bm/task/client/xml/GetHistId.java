/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import src.main.java.com.bm.task.client.connection.OSM_Query;
import src.main.java.com.bm.task.client.xml.XMLParser;


/**
 * Getting HistID from XMLAPI
 * @author Bhavya
 *
 */
public class GetHistId {
	
	XMLParser xmlParser;
	
	String userName;
	String password;
	
	String histId,task;
	String taskName;
	
	static final String KEY_ITEM = "Orderdata";
	static final String histSeqId = "_order_hist_seq_id";
	static final String taskId = "_task_id";
	
	public GetHistId(String userName,String password,String taskName) {
		this.userName=userName;
		this.password=password;
		this.taskName=taskName;
	}
	
	/**
	 * to get Hist_SEQ_ID
	 * @param id
	 * @return
	 */
	@SuppressWarnings("static-access")
	public String getHistoryID(String id)
	{
	OSM_Query osm_Query = new OSM_Query();
	osm_Query.user = userName;
	osm_Query.password = password;
	osm_Query.requestXml = "<Query.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
			+ "<OrderID>" + id + "</OrderID>" + "</Query.Request>";

	try {
		osm_Query.getQueryData();
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	xmlParser = new XMLParser();
	Document docForQuery = xmlParser.getDomElement(osm_Query.xmlResponse
			.toString());
	NodeList nlForHistId = docForQuery.getElementsByTagName(KEY_ITEM);
	xmlParser = new XMLParser();
	for (int position = 0; position < nlForHistId.getLength(); position++) {

		Element e = (Element) nlForHistId.item(position);
		task = xmlParser.getValue(e, taskId);
		if(task.equalsIgnoreCase(taskName))
		{
		histId = xmlParser.getValue(e, histSeqId);
		}

	}
	System.out.println("herw"+histId);
	return histId;
	}
}
