/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package src.main.java.com.bm.task.client.xml;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import src.main.java.com.bm.task.client.connection.OSM_Query;


/**
 * Reporting Values getting for Chart and display of Report
 * @author Bhavya
 *
 */
public class GetReportingValues {
	
	public static int flag=0;
	String userName;
	String password;
	
	int value;
	String xmlFileForReport;

	static final String KEY_ITEM = "Orderdata";
	
	public GetReportingValues(String userName, String password){
			this.userName=userName;
			this.password=password;

	}
	
	@SuppressWarnings("static-access")
	public int getReporting(String xml)
	{
		OSM_Query osm_Query = new OSM_Query();
		osm_Query.user = userName;
		osm_Query.password = password;
		osm_Query.requestXml = xml;
		String Result=null;

		try {
			Result = osm_Query.getQueryData();
		} catch (Exception e) {
			e.printStackTrace();
		}
			if (Result.equalsIgnoreCase("Failure")) {
				flag=1;

			} else {
				flag=0;

				StringBuffer resForReport = osm_Query.xmlResponse;
				xmlFileForReport = resForReport.toString();

				XMLParser xmlParser = new XMLParser();
				Document doc = xmlParser.getDomElement(xmlFileForReport);
				NodeList nl = doc.getElementsByTagName(KEY_ITEM);
				
				value = nl.getLength();

			}
			
			return value;
	}
	
	
}
