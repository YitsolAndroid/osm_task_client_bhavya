package com.bm.task.client.restWS;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bm.task.client.xml.RemarkDomain;

import src.main.java.com.bm.task.client.xml.XMLParser;

public class GetRemarksRWS {

	private static final String KEY_ITEM_Error = "error";
	private static final String KEY_ITEM_Remark = "Remark";
	private String xmlOutput;

	public static int flag;

	static final String KEY_ITEM_COMPLETE = "Completed";
	public static NodeList nlStatus;
	public static List<RemarkDomain> remarksList = new ArrayList<RemarkDomain>();
	private RemarkDomain remarkDomain;

	static final String text = "Text";
	static final String State = "State";
	static final String UserName = "Author";
	static final String TaskName = "TaskID";
	static final String TimeStamp = "Time";
	static final String Id = "Id";

	public GetRemarksRWS(String xmlOutput) {
		this.xmlOutput = xmlOutput;

	}

	public List<RemarkDomain> getRemarkRWS() {

		XMLParser xmlParser = new XMLParser();
		Document doc = xmlParser.getDomElement(xmlOutput);

		NodeList error = doc.getElementsByTagName(KEY_ITEM_Error);
		if (error.getLength() == 1) {
			flag = 1;
		} else {
			flag = 0;
			NodeList nl = doc.getElementsByTagName(KEY_ITEM_Remark);
			remarksList.clear();
			for (int position = 0; position < nl.getLength(); position++) {
				Element e = (Element) nl.item(position);
				remarkDomain = new RemarkDomain();
				remarkDomain.setId(xmlParser.getValue(e, Id));
				remarkDomain.setState(xmlParser.getValue(e, State));
				remarkDomain.setTaskName(xmlParser.getValue(e, TaskName));
				remarkDomain.setText(xmlParser.getValue(e, text));
				remarkDomain.setTimeStamp(xmlParser.getValue(e, TimeStamp));
				remarkDomain.setUserName(xmlParser.getValue(e, UserName));
				remarksList.add(remarkDomain);

			}

			
		}

		return remarksList;

	}
}
